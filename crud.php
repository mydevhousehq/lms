<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
// session_starter();
?>

<?php 
//request type
if($_SERVER['REQUEST_METHOD']=="POST"){

    if(isset($_POST['insert'])){
        //insert function 
        $title = $_POST['title'];
        $desc = $_POST['desc'];

        // insert function usage
        $array = array(
            'title'=>$title,
            'description' => $desc
        );
        if(insert($array,'tbl_test')){
            ?>
            <script>alert('record inserted');</script>
            <?php 
        }else{
            ?>
            <script>alert('record not inserted');</script>
            <?php 
        }

    }else{
        //wala lang
    }

    //delete
    if(isset($_POST['delete'])){
        $id = $_POST['delete'];
        // delete function usage
        if(delete($id ,'tbl_test')){
            ?>
            <script>alert('record  deleted');</script>
            <?php 
        }else{
            ?>
            <script>alert('record not deleted');</script>
            <?php 
        }
    }else{
        //wala lang
    }

    //update
    if(isset($_POST['update'])){
        $id = $_POST['update'];
        $title = $_POST['title'];
        $desc = $_POST['desc'];
        // update function usage
        $array = array(
        	'title'=>$title,
            'description' => $desc
        );
        if(update($array,$id,'tbl_test')){
        	?>
            <script>alert('record  updated');
             window.location.href = './crud.php';
            </script>
            <?php 
            
        }else{
        	?>
            <script>alert('record not updated');</script>
            <?php 
        }

    }else{
        //wala lang
    }

//
}
// end 
?>
<body class="<?php fileclass();?>">

<!-- insert  -->
<div class="container">
    <div class="row">
        <div class="col-md-6">
        <form action="<?php $_SERVER['PHP_SELF'];?>" method="post">
            <input type="hidden" name="insert" value="1">
            <div class="form-group">
                <label for="email">Title</label>
                <input type="text" name="title" class="form-control" placeholder="title" required>
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <textarea name="desc" class="form-control" id="" cols="30" rows="10"></textarea>
            </div>
            
            <button type="submit"  class="btn btn-primary">Submit</button>
        </form>
        </div>
    </div>
</div>
<!-- end insert  -->


<!-- read  -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!-- start table  -->
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>option</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                // GET usage
                $data = get('tbl_test');
                foreach ($data as $row) {
                   ?>
                    <tr>
                        <td><?php echo $row['id'];?></td>
                        <td><?php echo $row['title'];?></td>
                        <td><?php echo $row['description'];?></td>
                        <td>
                            <!-- delete  -->
                            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                            <input type="hidden" name="delete" value="<?php echo $row['id'];?>">
                            <button type="submit"  class="btn btn-danger">delete</button>
                            </form>

                            <!-- delete  -->
                            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="get">
                            <input type="hidden" name="updateview" value="<?php echo $row['id'];?>">
                            <button type="submit"  class="btn btn-warning">update</button>
                            </form>

                        </td>
                    </tr>
                   <?php 
                }
                ?>
                </tbody>
            </table>
            <!-- end table  -->
        </div>
    </div>
</div>
<!-- end read  -->


<!-- update view -->

<?php 
if(isset($_GET['updateview'])){
?>
   <div class="container">
       <div class="row">
           <div class="col-md-6">

                <?php 
                // get where field
                $data = get_where_fieldvalue('tbl_test','id',$_GET['updateview']);
                foreach ($data as $row) {
                   ?>
                <!-- start form  -->
                <form action="<?php $_SERVER['PHP_SELF'];?>" method="post">
                    <input type="hidden" name="update" value="<?php echo $row['id'];?>">
                    <div class="form-group">
                        <label for="email">Title</label>
                        <input type="text" name="title" value="<?php echo $row['title'];?>" class="form-control" placeholder="title" required>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <textarea name="desc" class="form-control" id="" cols="30" rows="10"><?php echo $row['description'];?></textarea>
                    </div>
                    
                    <button type="submit"  class="btn btn-primary">Submit</button>
                </form>
                <!-- end form  -->
                   <?php 
                }
                ?>

               

           </div>
       </div>
   </div>
<?php 
}
?>

<!-- end update view  -->

 <?php include($partials.'footer.php');?>