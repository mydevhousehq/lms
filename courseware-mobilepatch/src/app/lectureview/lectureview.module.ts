import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LectureviewPageRoutingModule } from './lectureview-routing.module';

import { LectureviewPage } from './lectureview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LectureviewPageRoutingModule
  ],
  declarations: [LectureviewPage]
})
export class LectureviewPageModule {}
