import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LectureviewPage } from './lectureview.page';

describe('LectureviewPage', () => {
  let component: LectureviewPage;
  let fixture: ComponentFixture<LectureviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LectureviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LectureviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
