import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LectureviewPage } from './lectureview.page';

const routes: Routes = [
  {
    path: '',
    component: LectureviewPage
  },
  {
    path: ':id',
    component: LectureviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LectureviewPageRoutingModule {}
