
import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  public datax : Array<any> = [];
  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit() {
    // this.getdata();
    this.rest.route('announcement').subscribe(res => {
      this.datax = res;
      console.log(this.datax);
    });
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
