import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  public api = environment.apiurl;
  constructor(private http: HttpClient) { }
  
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }


  //get data
  getlecture(): Observable<any> {
    const apiurl = this.api+'lecture.php';
    // console.log('apiurl::::::::'+apiurl);
    return this.http.get(apiurl).pipe(
      map(this.extractData));
  }
  //get data
  students(): Observable<any> {
    const apiurl = this.api+'students.php';
     console.log('apiurl::::::::'+apiurl);
    return this.http.get(apiurl).pipe(
      map(this.extractData));
  }

  //data + id
getlectureid(id): Observable<any> {
  const apiurl = this.api + 'lectureid.php?id='+id ;
  console.log('apiurl::::::::'+apiurl);
  return this.http.get(apiurl).pipe(
    map(this.extractData));
}

  //data + id
  route(route): Observable<any> {
    const apiurl = this.api + route + ".php" ;
    console.log('apiurl::::::::'+apiurl);
    return this.http.get(apiurl).pipe(
      map(this.extractData));
  }

}
