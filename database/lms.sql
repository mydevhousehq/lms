-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 28, 2020 at 10:07 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nstp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_announcement`
--

CREATE TABLE `tbl_announcement` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `descript` text NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_announcement`
--

INSERT INTO `tbl_announcement` (`id`, `title`, `descript`, `date`) VALUES
(5, 'Prelim exam', 'The Prelim exam will be given in class on Week 5 of the semester.. Please take your Quiz 1 and 2 Before taking your Preliminary Exams', '2020-09-27'),
(6, 'Midterms', 'Students who are taking the NSTP subject should be advised that their first Midterm Exam is scheduled on Week 10 of this Semester. Please take your Quiz 3 and 4 before taking Your Midterm Exams. Thank you', '2020-10-31'),
(8, 'finals ', 'The final exam for NSTP will be held on the last week of the Semester, There is only one exam period, so if you finish early you are permitted to leave early. Make sure that you take all your quizzes first. Please secure your clearance and permit before taking your final exam. Note that final exam conflicts are only handled and scheduled by the MIT Registrars Office.', '2020-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam`
--

CREATE TABLE `tbl_exam` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_exam`
--

INSERT INTO `tbl_exam` (`id`, `title`, `type`) VALUES
(19, 'Quiz 1', 'Q1'),
(20, 'Quiz 2', 'Q2'),
(21, 'Prelims', 'prelim'),
(22, 'Quiz 3', 'Q3'),
(23, 'Quiz 4', 'Q4'),
(24, 'Quiz 5', 'Q5'),
(25, 'Quiz 6', 'Q6'),
(26, 'Midterms', 'midterm'),
(27, 'Finals', 'finals');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_item`
--

CREATE TABLE `tbl_exam_item` (
  `id` int(11) NOT NULL,
  `qid` text NOT NULL,
  `question` text NOT NULL,
  `correct` text NOT NULL,
  `a` text NOT NULL,
  `b` text NOT NULL,
  `c` text NOT NULL,
  `d` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_exam_item`
--

INSERT INTO `tbl_exam_item` (`id`, `qid`, `question`, `correct`, `a`, `b`, `c`, `d`) VALUES
(22, '19', 'Refers to the pro program aimed at enhancing civic consciousness and defense preparedness in the youth, by developing the ethics of service and patriotism.', 'a', 'ROTC', 'NSTP', 'LTS', 'CWTS'),
(23, '19', 'Designed to provide military training to tertiary level students in order to motivate, train, organize and mobilize them for national defense preparedness.', 'a', 'ROTC', 'NSTP', 'LTS', 'CWTS'),
(24, '19', 'Refers to the Program component designed to train the students to teach literacy and numeracy skills to school children, out-of-school youths and other segments of society in need of their services.', 'c', 'NSTP', 'ROTC', 'LTS', 'CWTS'),
(25, '19', 'Refers to the Program component or activities contributory to the general welfare and the betterment of life for the members of the community or the enhancement of its facilities, especially those devoted to improving health, education, environment, entrepreneurship, safety, recreation and moral of the citizenry and other social welfare services.', 'b', 'NSTP', 'CWTS', 'LTS', 'ROTC'),
(26, '19', 'No fees shall be collected for any of the NSTP components except basic tuition which should be more than fifty (50%) percent of the charges of the school per academic unit.  NSTP tuition collected shall constitute a Trust Fund, which shall be exclusively used for the operation of the Program.', 'b', 'True', 'False', ' ', ' '),
(27, '19', '__________ Refers to the grouping of students enrolled to different schools and taking up the same NSTP component into one group under the management and supervision of a designated school.', 'd', 'NSTP', 'CROSS ENROLLMENT', 'NON-GOVERNMENT ORGANIZATION', 'CLUSTERING'),
(28, '19', '__________ Refers to a system of enrollment were a student is officially enrolled in an academic program of an origin school but is allowed to enroll in the NSTP component of another accepting school', 'a', 'CROSS ENROLLMENT', 'CUSTERING', 'NON-GOVERNMENT ORGANIZATION', 'ROTC'),
(29, '19', 'Within thirty (30) days from the approval of this revised IRR, the CHED, TESDA, and the DND shall jointly issue the minimum standards for the three (3) NSTP components which shall form part of these guidelines.', 'a', 'True', 'False', ' ', ' '),
(30, '19', 'Graduates of the non-ROTC components of the NSTP shall belong to the _____________.', 'c', 'PMA', 'CWTS', 'NATIONALSERVICE RESERVE CORPS', 'NSTP'),
(31, '19', 'The National Service Training Program (NSTP) is a Law otherwise known as Republic Act 9163 or the NSTP Act of 2001.', 'a', 'True', 'False', ' ', ' '),
(32, '20', '___________ is having a clear perception of your personality, including strengths, weaknesses, thoughts, beliefs, motivation, and emotions. ', 'a', 'SELF- AWARENESS', 'SELF-CONCEPT', 'EXTERNAL SELF- AWARENESS', 'INTERNAL SELF- CONCEPT'),
(33, '20', '___________ represents how clearly we see our own values, passions, aspirations, fit with our environment, reactions (including thoughts, feelings, behaviors, strengths, and weaknesses), and impact on others.', 'd', 'SELF- AWARENESS', 'SELF-CONCEPT', 'EXTERNAL SELF- AWARENESS', 'INTERNAL SELF- CONCEPT'),
(34, '20', '__________ means understanding how other people view us.', 'c', 'SELF- AWARENESS', 'SELF-CONCEPT', 'EXTERNAL SELF- AWARENESS', 'INTERNAL SELF- CONCEPT'),
(35, '20', 'Theyâ€™re clear on who they are but donâ€™t challenge their own views or search for blind spots by getting feedback from others. This can harm their relationships and limit their success.', 'd', 'PLEASERS', 'AWARE', 'SEEKERS', 'INTROSPECTORS'),
(36, '20', 'They donâ€™t yet know who they are, what they stand for, or how their teams see them. As a result, they might feel stuck or frustrated with their performance and relationships.', 'a', 'SEEKERS', 'INTROSPECTORS', 'AWAKER', 'PLEASERS'),
(37, '20', 'They know who they are, what they want to accomplish, and seek out and value othersâ€™ opinions. This is where leaders begin to fully realize the true benefits of self-awareness.', 'd', 'SEEKERS', 'INTROSPECTORS', 'PLEASERS', 'AWARE'),
(38, '20', 'They can be so focused on appearing a certain way to others that they could be overlooking what matters to them. Over time, they tend to make choices that arenâ€™t in service of their own success and fulfilment.', 'd', 'INTROSPECTORS', 'SEEKERS', 'AWARE', 'PLEASERS'),
(39, '20', '___________ is generally thought of as our individual perceptions of our behavior, abilities, and unique characteristics', 'b', 'SELF- AWARENESS', 'SELF-CONCEPT', 'SELF-ESTEEM', 'IDEAL SELF'),
(40, '20', '__________ is the ability to interact with others.', 'a', 'SOCIAL', 'PHYSICAL', 'CHAT', 'PLEASERS'),
(41, '20', 'The symptoms of Superiority Complex stated below are True except:', 'c', 'UNWILLINGNESS TO LISTEN TO OTHERS', 'HIGH VALUATIONS OF SELF-WORTH', 'EXCESSIVE SHOWING OFF', 'd.	BOASTFUL CLAIMS THAT ARENâ€™T BACKED UP BY REALITY'),
(42, '21', 'You value peace by doing well for others and for your country living and working together in harmony and avoiding violence as a way of setting disputes.', 'a', 'The Good Citizenship Value of Peace', 'The Good Citizenship Value of Freedom', 'The Good Citizenship Value of Love', 'The Good Citizenship Value of Truth'),
(43, '21', 'A ___________ is worth more than any worldly gain.', 'b', 'FRIENDS', 'FAMILY', 'CLASSMATES', 'GIRLFRIEND'),
(44, '21', '_____________ means serenity of mind simplicity of heart and tranquility of soul.', 'd', 'HAPPINESS', 'PRIDE', 'SELF-ESTEEM', 'PEACE'),
(45, '21', '______________ is a gift and it has become part of your nature destiny. God has given you talents to use as an investments in your work and in return you are expected to settle for nothing less than excellent results.', 'c', 'PEACE', 'JOY', 'WORK', 'SELF'),
(46, '21', 'The goals of MDGs stated below are True except:', 'd', 'TO ERADICATE EXTREME POVERTY AND HUNGER', 'TO COMBAT HIV/AIDS, MALARIA, AND OTHER DISEASES', 'TO ENSURE ENVIRONMENTAL SUSTAINABILITY', 'NONE OF THE ABOVE'),
(47, '21', 'Able to put attention on your emotions, and physical state in a way to relax and thereby ', 'c', 'BASIC LEVEL OF SELF- AWARENESS', 'MEDIUM LEVEL OF SELF- AWARENESS', 'HIGH LEVEL OF SELF- AWARENESS', 'NONE OF THE ABOVE'),
(48, '21', 'NSTP funds derived from NSTP-related operations shall serve as augmentation to sustain un-programmed activities of NSTP.', 'a', 'True', 'False', ' ', ' '),
(49, '21', 'State Universities and Colleges (SUCs), shall offer the ROTC   component and at least one (1) other NSTP component   ', 'a', 'True', 'False', ' ', ' '),
(50, '21', 'A program of assistance/incentives for ROTC students shall not be provided and administered by DND, in accordance with existing laws and regulations and subject to the availability of funds.', 'b', 'True', 'False', ' ', ' '),
(51, '21', 'Graduates of the ROTC program shall form part of the Citizen Armed Force pursuant to RA 7077, subject to the requirements of DND', 'a', 'True', 'False', ' ', ' '),
(52, '22', '_____________ is a process by which a person influences others to accomplish an objective or task, or sway their decision and opinions.', 'a', 'LEADERSHIP', 'BOSS', 'FOLLOWER', 'NONE OF THE ABOVE'),
(53, '22', 'Must have an honest understanding of who you are, what you know, and what you can do, as a leader to be able to lead effectively.', 'b', 'LEADERSHIP', 'LEADER', 'FOLLOWER', 'SITUATION'),
(54, '22', 'They are the direct recipient of leadership', 'c', 'FANS', 'ARMY', 'FOLLOWERS', 'STALKER'),
(55, '22', 'Must always use your judgment to decide the best course of action and style needed for each situation.', 'c', 'JUDGMENT', 'ACTION', 'SITUATION', 'STYLE'),
(56, '22', 'This would have to be one of the biggest influencers in the workplace. As a leader, your influence will carry great weight. If you choose to congratulate and encourage or to order and criticise, your choice will clearly set the tone of your leadership.', 'c', 'EMPOWER AND DEVELOP OTHERS', 'GRATITUDE', 'POSITIVE COMMUNICATION', 'TAKE RESPONSIBILITY'),
(57, '22', 'Helps establish roles and responsibilities for each employee, but it can also encourage bare-minimum work if employees know how much their effort is worth all the time.', 'd', 'DEMOCRATIC LEADERSHIP', 'AUTOCRATIC LEADERSHIP', 'LAISSEZ-FAIRE LEADERSHIP', 'TRANSACTIONAL LEADERSHIP'),
(58, '22', 'It focuses on identifying and nurturing the individual strengths of each member on his or her team.', 'a', 'COACH STYLE LEADERSHIP', 'DEMOCRATIC LEADERSHIP', 'DEMOCRATIC LEADERSHIP', 'TRANSFORMATIONAL LEADERSHIP'),
(59, '22', '___________________ is always transforming and improving upon the companys conventions.', 'b', 'DEMOCRATIC LEADERSHIP', 'TRANSFORMATIONAL LEADERSHIP', 'LAISSEZ-FAIRE LEADERSHIP', 'COACH STYLE LEAADERSHIP'),
(60, '22', 'He or she accepts the burden of executive interests while ensuring that current working conditions remain stable for everyone else.', 'd', 'DEMOCRATIC LEADERSHIP', 'TRANSFORMATIONAL LEADERSHIP', 'BUREAUCRATIC LEADERSHIP', 'STRATEGIC LEADERSHIP'),
(61, '22', 'The inverse of democratic leadership.', 'd', 'BUREAUCRATIC LEADERSHIP', 'TRANSACTIONAL LEADERSHIP', 'TRANSFORMAL LEADERSHIP', 'AUTOCRATIC LEADERSHIP'),
(62, '23', 'Means by which people choose their officials for definite and fixed periods and to whom they entrust, for the time being as representatives, the exercise of powers of government.', 'b', 'PLEBISCITE', 'ELECTION', 'POLITICAL RIGHT', 'REFERENDUM'),
(63, '23', 'The submission of a law by the national or local legislative to the voting citizens of a country for their ratification.', 'c', 'PLEBISCITE', 'POLITICAL RIGHT', 'REFERENDUM', 'RECALL'),
(64, '23', 'It is the process whereby the people directly propose and enact laws.', 'a', 'INITIATIVE', 'SUFFRAGE', 'ELECTION', 'REFERENDUM'),
(65, '23', 'The vote of the people expressing their choice for or against a proposed law or enactment submitted to them.', 'd', 'RECALL', 'REFERENDUM', 'ELECTION', 'PLEBISCITE'),
(66, '23', '____________________ are household and workplace chemicals that people misuse to become intoxicated.', 'b', 'CONTROLLED SUBSTANCES', 'VOLATILE SUBSTANCES', 'DANGEROUS DRUGS', 'NONE OF THE ABOVE'),
(67, '23', '___________________ include opioids, stimulants, depressants, hallucinogens, and anabolic steroids.', 'a', 'CONTROLLED SUBSTANCES', 'VOLATILE SUBTANCES', 'ALL OF THE ABOVE', 'NONE OF THE ABOVE'),
(68, '23', '__________________ is an antipsychotic medication used to treat schizophrenia.', 'b', 'CLARITHROMYCIN', 'CLOZAPINE', 'DIGOXIN', 'BROMOCRIPTINE'),
(69, '23', '__________________ is otherwise known as â€œECSTASYâ€.', 'a', 'METHYLENEDIOXYMETHAMPHETAMINE', 'METHAMPHETAMINE HYDROCHLORIDE', 'ACETAMINOPHEN', 'COLCHICINE'),
(70, '23', '________________ is locally known as â€œMARIJUANAâ€', 'd', 'BENZODIAZEPINES', 'SALVIA DIVINORIUM', 'METHAMPHETAMINE HYDROCHLORIDE', 'CANNABIS'),
(71, '23', 'Any person who is a licensed physician, dentist, chemist, medical technologist, nurse, midwife, veterinarian or pharmacist in the Philippines.', 'c', 'FINANCIER', 'PUSHER', 'PRACTITIONER', 'PROTECTOR'),
(72, '24', 'They slow the messages going to and from your brain.', 'a', 'Depressants', 'Hallucinogens', 'Stimulant', 'Analgesic'),
(73, '24', 'They speed up messaging to and from the brain, making you feel more alert and confident.', 'c', 'Depressants', 'Hallucinogens', 'Stimulant', 'Analgesic'),
(74, '24', 'The implementing arm of DBB. It is responsible for the efficient and effective enforcement of the provisions on any dangerous drug and/ or other known harmful substances that were mentioned in R.A. No. 9165.', 'd', 'DARN', 'NBI', 'DBB', 'PDEA'),
(75, '24', 'An active chemical component in food that plays a specific structural or functional role in the bodyâ€™s activity which provides energy, helps to grow well, and normal development repair tissues.', 'd', 'Diet', 'Nutrition', 'Food', 'Nutrients'),
(76, '24', 'The major component of food which is the main source of energy.', 'a', 'Carbohydrate', 'Protein', 'Nutrients', 'Calories'),
(77, '24', 'Mandated to be the over-all technical authority on health.', 'b', 'DARN', 'DOH', 'DSWD', 'NBI'),
(78, '24', 'Tends to stop your body producing enough urine, so your body retains fluid.', 'd', 'Cannabis', 'Cocaine', 'Ice', 'Ecstasy'),
(79, '24', 'If injected, can cause vein collapse and increases the risk of HIV and hepatitis infection.\r\na.	Amphetamine\r\n', 'd', 'Pills', 'Amphetamine', 'Cannabis', 'Mephedrone'),
(80, '24', 'Can make you feel alert, warm and chatty.', 'b', 'Cannabis', 'Ecstasy', 'Mephedrone', 'Cocaine'),
(81, '24', 'This is a process of eliminating a personâ€™s dependence on drugs.', 'c', 'Harm Reduction', 'Drug Rehabilitation', 'Detoxification', 'NONE OF THE ABOVE'),
(82, '25', 'The sequence and balance of meals in a day.', 'b', 'Nutrition', 'Diet', 'Food', 'NONE OF THE ABOVE'),
(83, '25', 'The combination of processes by which the living organism receives and uses the food materials necessary for growth, maintenance of functions, and repair of components parts.', 'b', 'Food', 'Nutrition', 'Diet', 'Nutrient'),
(84, '25', 'An active chemical component in food that plays a specific structural or functional role in the bodyâ€™s activity which provides energy, helps to grow well, and normal development repair tissues. Organic and inorganic complexes contained in food are called _____________________.', 'd', 'Food', 'Nutrition', 'Diet', 'Nutrient'),
(85, '25', 'It is the major component of food which is the main source of energy.', 'c', 'Nutrient', 'Protein', 'Carbohydrate', 'Fats'),
(86, '25', 'It acts as Building blocks of cells and tissues.', 'b', 'Nutrient', 'Protein', 'Carbohydrate', 'NONE OF THE ABOVE'),
(87, '25', 'It regulates hemoglobin and muscle contraction, the formation of enzyme, hormones, and other secretions which help the synthesis of enzymes and produces digestive juices and antibodies.', 'b', 'Nutrient', 'Protein', 'Carbohydrate', 'NONE OF THE ABOVE'),
(88, '25', 'It is the process of keeping places free from dirt, infection and diseases by removing waste trash and garbage, by cleaning the streets, washing yourself and have a safe drinking water.', 'c', 'Hygiene', 'Cleanliness', 'Sanitation', 'Disinfection'),
(89, '25', '________________ is an airborne virus and can spread through small droplets of saliva in a similar way to the cold and influenza.', 'b', 'Tuberculosis', 'SARS', 'HIV', 'AIDS'),
(90, '25', 'It is a chronic, potentially life-threatening condition caused by the human immunodeficiency virus (HIV).', 'c', 'Tuberculosis', 'ESRD', 'AIDS', 'HIV'),
(91, '25', 'A regulator of all health services and products; and provider of special or tertiary health care services and of technical assistance to other health providers especially to Local Government Units (LGU).', 'b', 'DSWD', 'DOH', 'WHO', 'NONE OF THE ABOVE'),
(92, '26', '_____________ is a process by which a person influences others to accomplish an objective or task, or sway their decision and opinions.', 'a', 'LEADERSHIP', 'LEADER', 'FOLLOWER', 'BOSS'),
(93, '26', 'Must have an honest understanding of who you are, what you know, and what you can do, as a leader to be able to lead effectively.', 'a', 'LEADER', 'FOLLOWERS', 'LEADERSHIP', 'SITUATION'),
(94, '26', 'Must always use your judgment to decide the best course of action and style needed for each situation.', 'b', 'JUDGMENT', 'SITUATION', 'ACTION', 'STYLE'),
(95, '26', 'This includes understanding non-verbal cues, but also being able to use reflection and clarification to your advantage.', 'd', 'THE ABILITY TO ENCOURAGE', 'THE ABILITY TO SYMPATHIZE AND TO EMPATHIZE', 'AN EMPHATHETIC NATURE', 'THE ABILITY TO LISTEN'),
(96, '26', 'Have the ability to understand other peoples emotions', 'b', 'THE ABILITY TO ENCOURAGE', 'THE ABILITY TO SYMPATHIZE AND TO EMPATHIZE', 'AN EMPATHETIC NATURE', 'THE ABILITY TO LISTEN'),
(97, '26', 'The inverse of democratic leadership.', 'd', 'BUREAUCRATIC LEADERSHIP', 'TRANSACTIONAL LEADERSHIP', 'TRANSFORMAL LEADERSHIP', 'AUTOCRATIC LEADERSHIP'),
(98, '26', 'Employees are neither considered nor consulted prior to a direction, and are expected to adhere to the decision at a time and pace stipulated by the leader', 'b', 'LAISSEZ- FAIRE LEADERSHIP', 'AUTOCRATIC LEADERSHIP', 'STRATEGIC LEADERSHIP', 'COACH STYLE LEADERSHIP'),
(99, '26', 'He or she accepts the burden of executive interests while ensuring that current working conditions remain stable for everyone else.', 'd', 'DEMOCRATIC LEADERSHIP', 'TRANSFORMATIONAL LEADERSHIP', 'BUREAUCRATIC LEADERSHIP', 'STRATEGIC LEADERSHIP'),
(100, '26', '___________________ is always transforming and improving upon the companys conventions.', 'b', 'DEMOCRATIC LEADERSHIP', 'TRANSFORMATIONAL LEADERSHIP', 'LAISSEZ-FAIRE LEADERSHIP', 'COACH STYLE LEAADERSHIP'),
(101, '26', 'Means by which people choose their officials for definite and fixed periods and to whom they entrust, for the time being as representatives, the exercise of powers of government.', 'b', 'PLEBISCITE', 'ELECTION', 'POLITICAL RIGHT', 'REFERENDUM'),
(102, '27', 'They slow the messages going to and from your brain.', 'b', 'Hallucinogens', 'Depressants', 'Stimulant', 'Analgesic'),
(103, '27', 'Distort the sense of reality. You may see or hear things that are not really there, or see things in a distorted way.', 'd', 'Depressants', 'Stimulant', 'Analgesic', 'Hallucinogens'),
(104, '27', 'Tends to stop your body producing enough urine, so your body retains fluid.', 'd', 'Cannabis', 'Cocaine', 'Ecstacy', 'Ice'),
(105, '27', 'They speed up messaging to and from the brain, making you feel more alert and confident.', 'c', 'Depressants', 'Hallucinogens', 'Stimulant', 'Analgesic'),
(106, '27', 'Term for the processes of medical or psychotherapeutic treatment, for dependency on psychoactive substances such as alcohol, prescription drugs, and street drugs such as cocaine, heroin or amphetamines.', 'b', 'Detoxification', 'Drug Rehabilitation', 'Harm Reduction', 'NONE OF THE ABOVE'),
(107, '27', 'This is a process of eliminating a personâ€™s dependence on drugs.', 'c', 'Harm Reduction', 'Drug Rehabilitation', 'Detoxification', 'NONE OF THE ABOVE'),
(108, '27', 'It provides a comprehensive rehabilitation and educational program for drug victims in order to prevent and control drug abuse in the country.', 'a', 'DARN', 'NBI', 'DBB', 'DOH'),
(109, '27', 'Refer to a very broad range of activities aimed at reducing the risk of drug use among non-users and assuring continued non- use.', 'a', 'Primary Prevention ', 'Secondary Prevention ', 'Tertiary Prevention ', 'NONE OF THE ABOVE'),
(110, '27', 'The implementing arm of DBB. It is responsible for the efficient and effective enforcement of the provisions on any dangerous drug and/ or other known harmful substances that were mentioned in R.A. No. 9165.', 'c', 'DARN', 'NBI', 'PDEA', 'DOH'),
(111, '27', 'The sequence and balance of meals in a day.', 'b', 'Nutrition', 'Diet', 'Food', 'NONE OF THE ABOVE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lecture`
--

CREATE TABLE `tbl_lecture` (
  `id` int(11) NOT NULL,
  `phase` int(11) NOT NULL,
  `content` text NOT NULL,
  `video` text NOT NULL,
  `file` text NOT NULL,
  `file2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_lecture`
--

INSERT INTO `tbl_lecture` (`id`, `phase`, `content`, `video`, `file`, `file2`) VALUES
(19, 1, 'Module 1: Orientation', 'https://www.youtube.com/watch?v=HRzxFR0g5-E&t=2s', 'week 01.pdf', 'Week 01.pdf'),
(20, 2, 'Module 02: Organization and Orientation', 'https://www.youtube.com/watch?v=bpfYkhqt_pk', 'week 02.pdf', 'Week 02.pdf'),
(21, 3, 'Module 03: Self-Awareness and Values Development ', 'https://www.youtube.com/watch?v=rOGHFt4jVI8', 'week 03.pdf', 'Week 03.pdf'),
(22, 4, 'Module 04: Values Development for Good Citizenship', 'https://www.youtube.com/watch?v=_vHzUyVcnb8', 'week 04.pdf', 'Week 04.pdf'),
(23, 6, 'Module 06: Application of the Good Citizenship Core Values in Daily Life', 'https://www.youtube.com/watch?v=Rq31NL7nGmg&t=67s', 'week 06.pdf', 'Week 06.pdf'),
(24, 7, 'Module 07: Basic Leadership Training', 'https://www.youtube.com/watch?v=lUJso5iqlcU', 'week 07.pdf', 'Week 07.pdf'),
(25, 8, 'Module 08: Dimensions of Development', 'https://www.youtube.com/watch?v=JEGwarCz3qQ', 'week 08.pdf', 'Week 08.pdf'),
(26, 9, 'Module 09: Health', 'https://www.youtube.com/watch?v=CtWWUSvNwXo&t=11s', 'week 09.pdf', 'Week 09.pdf'),
(27, 12, '   Module 12: The Governmentâ€™s Counter-Action against Drug Abuse ', 'https://www.youtube.com/watch?v=1Ri10Z7NZWU&t=57s', 'week 12.pdf', 'Week 12.pdf'),
(28, 13, 'Module 13: Safety Education and Sports and Recreation \r\n', 'https://www.youtube.com/watch?v=PEi4x-PPwqk&t=46s', 'week 13.pdf', 'Week 13.pdf'),
(30, 14, 'THIS SYSTEM LIMIT TO WEEK 14 ONLY, YOU CANNOT ADD WEEK 15 ANYMORE!!!', 'https://www.youtube.com/watch?v=ZyhrYis509A', 'GettyImages-843466180-800x526.png', 'GettyImages-843466180-800x526.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_answer`
--

CREATE TABLE `tbl_student_answer` (
  `id` int(11) NOT NULL,
  `qid` text NOT NULL,
  `item_id` text NOT NULL,
  `student_answer` text NOT NULL,
  `correct_answer` text NOT NULL,
  `student_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_student_answer`
--

INSERT INTO `tbl_student_answer` (`id`, `qid`, `item_id`, `student_answer`, `correct_answer`, `student_id`) VALUES
(1, '19', '22', 'a', 'a', 'STUD-70'),
(2, '19', '23', 'b', 'a', 'STUD-70'),
(3, '19', '24', 'd', 'c', 'STUD-70'),
(4, '19', '25', 'd', 'b', 'STUD-70'),
(5, '19', '26', 'a', 'b', 'STUD-70'),
(6, '19', '27', 'b', 'd', 'STUD-70'),
(7, '19', '28', 'c', 'a', 'STUD-70'),
(8, '19', '29', 'd', 'a', 'STUD-70'),
(9, '19', '30', 'a', 'c', 'STUD-70'),
(10, '19', '31', 'a', 'a', 'STUD-70'),
(11, '19', '22', 'b', 'a', 'STUD-69'),
(12, '19', '23', 'a', 'a', 'STUD-69'),
(13, '19', '24', 'd', 'c', 'STUD-69'),
(14, '19', '25', 'c', 'b', 'STUD-69'),
(15, '19', '26', 'a', 'b', 'STUD-69'),
(16, '19', '27', 'd', 'd', 'STUD-69'),
(17, '19', '28', 'a', 'a', 'STUD-69'),
(18, '19', '29', 'a', 'a', 'STUD-69'),
(19, '19', '30', 'c', 'c', 'STUD-69'),
(20, '19', '31', 'a', 'a', 'STUD-69'),
(21, '20', '32', 'a', 'a', 'STUD-69'),
(22, '20', '33', 'b', 'd', 'STUD-69'),
(23, '20', '34', 'c', 'c', 'STUD-69'),
(24, '20', '35', 'a', 'd', 'STUD-69'),
(25, '20', '36', 'b', 'a', 'STUD-69'),
(26, '20', '37', 'c', 'd', 'STUD-69'),
(27, '20', '38', 'd', 'd', 'STUD-69'),
(28, '20', '39', 'c', 'b', 'STUD-69'),
(29, '20', '40', 'b', 'a', 'STUD-69'),
(30, '20', '41', 'd', 'c', 'STUD-69'),
(31, '21', '42', 'a', 'a', 'STUD-69'),
(32, '21', '43', 'a', 'b', 'STUD-69'),
(33, '21', '44', 'd', 'd', 'STUD-69'),
(34, '21', '45', 'c', 'c', 'STUD-69'),
(35, '21', '46', 'c', 'd', 'STUD-69'),
(36, '21', '47', 'c', 'c', 'STUD-69'),
(37, '21', '48', 'a', 'a', 'STUD-69'),
(38, '21', '49', 'a', 'a', 'STUD-69'),
(39, '21', '50', 'b', 'b', 'STUD-69'),
(40, '21', '51', 'b', 'a', 'STUD-69'),
(41, '21', '42', 'b', 'a', 'STUD-06'),
(42, '21', '43', 'b', 'b', 'STUD-06'),
(43, '21', '44', 'd', 'd', 'STUD-06'),
(44, '21', '45', 'a', 'c', 'STUD-06'),
(45, '21', '46', 'c', 'd', 'STUD-06'),
(46, '21', '47', 'b', 'c', 'STUD-06'),
(47, '21', '48', 'c', 'a', 'STUD-06'),
(48, '21', '49', 'a', 'a', 'STUD-06'),
(49, '21', '50', 'b', 'b', 'STUD-06'),
(50, '21', '51', 'a', 'a', 'STUD-06'),
(51, '27', '102', 'b', 'b', 'STUD-06'),
(52, '27', '103', 'd', 'd', 'STUD-06'),
(53, '27', '104', 'b', 'd', 'STUD-06'),
(54, '27', '105', 'a', 'c', 'STUD-06'),
(55, '27', '106', 'b', 'b', 'STUD-06'),
(56, '27', '107', 'c', 'c', 'STUD-06'),
(57, '27', '108', 'a', 'a', 'STUD-06'),
(58, '27', '109', 'b', 'a', 'STUD-06'),
(59, '27', '110', 'c', 'c', 'STUD-06'),
(60, '27', '111', 'b', 'b', 'STUD-06'),
(61, '19', '22', 'b', 'a', 'STUD-06'),
(62, '19', '23', 'a', 'a', 'STUD-06'),
(63, '19', '24', 'c', 'c', 'STUD-06'),
(64, '19', '25', 'b', 'b', 'STUD-06'),
(65, '19', '26', 'b', 'b', 'STUD-06'),
(66, '19', '27', 'd', 'd', 'STUD-06'),
(67, '19', '28', 'a', 'a', 'STUD-06'),
(68, '19', '29', 'a', 'a', 'STUD-06'),
(69, '19', '30', 'c', 'c', 'STUD-06'),
(70, '19', '31', 'a', 'a', 'STUD-06'),
(71, '20', '32', 'a', 'a', 'STUD-06'),
(72, '20', '33', 'b', 'd', 'STUD-06'),
(73, '20', '34', 'c', 'c', 'STUD-06'),
(74, '20', '35', 'b', 'd', 'STUD-06'),
(75, '20', '36', 'a', 'a', 'STUD-06'),
(76, '20', '37', 'b', 'd', 'STUD-06'),
(77, '20', '38', 'a', 'd', 'STUD-06'),
(78, '20', '39', 'b', 'b', 'STUD-06'),
(79, '20', '40', 'a', 'a', 'STUD-06'),
(80, '20', '41', 'b', 'c', 'STUD-06'),
(81, '22', '52', 'c', 'a', 'STUD-06'),
(82, '22', '53', 'b', 'b', 'STUD-06'),
(83, '22', '54', 'c', 'c', 'STUD-06'),
(84, '22', '55', 'a', 'c', 'STUD-06'),
(85, '22', '56', 'c', 'c', 'STUD-06'),
(86, '22', '57', 'c', 'd', 'STUD-06'),
(87, '22', '58', 'a', 'a', 'STUD-06'),
(88, '22', '59', 'd', 'b', 'STUD-06'),
(89, '22', '60', 'b', 'd', 'STUD-06'),
(90, '22', '61', 'd', 'd', 'STUD-06'),
(91, '22', '52', 'a', 'a', 'STUD-69'),
(92, '22', '53', 'b', 'b', 'STUD-69'),
(93, '22', '54', 'c', 'c', 'STUD-69'),
(94, '22', '55', 'a', 'c', 'STUD-69'),
(95, '22', '56', 'c', 'c', 'STUD-69'),
(96, '22', '57', 'd', 'd', 'STUD-69'),
(97, '22', '58', 'c', 'a', 'STUD-69'),
(98, '22', '59', 'a', 'b', 'STUD-69'),
(99, '22', '60', 'c', 'd', 'STUD-69'),
(100, '22', '61', 'c', 'd', 'STUD-69'),
(101, '23', '62', 'a', 'b', 'STUD-69'),
(102, '23', '63', 'b', 'c', 'STUD-69'),
(103, '23', '64', 'd', 'a', 'STUD-69'),
(104, '23', '65', 'c', 'd', 'STUD-69'),
(105, '23', '66', 'b', 'b', 'STUD-69'),
(106, '23', '67', 'a', 'a', 'STUD-69'),
(107, '23', '68', 'c', 'b', 'STUD-69'),
(108, '23', '69', 'a', 'a', 'STUD-69'),
(109, '23', '70', 'd', 'd', 'STUD-69'),
(110, '23', '71', 'c', 'c', 'STUD-69'),
(111, '23', '62', 'c', 'b', 'STUD-06'),
(112, '23', '63', 'b', 'c', 'STUD-06'),
(113, '23', '64', 'd', 'a', 'STUD-06'),
(114, '23', '65', 'a', 'd', 'STUD-06'),
(115, '23', '66', 'b', 'b', 'STUD-06'),
(116, '23', '67', 'a', 'a', 'STUD-06'),
(117, '23', '68', 'b', 'b', 'STUD-06'),
(118, '23', '69', 'a', 'a', 'STUD-06'),
(119, '23', '70', 'd', 'd', 'STUD-06'),
(120, '23', '71', 'c', 'c', 'STUD-06'),
(121, '24', '72', 'a', 'a', 'STUD-69'),
(122, '24', '73', 'c', 'c', 'STUD-69'),
(123, '24', '74', 'a', 'd', 'STUD-69'),
(124, '24', '75', 'b', 'd', 'STUD-69'),
(125, '24', '76', 'c', 'a', 'STUD-69'),
(126, '24', '77', 'b', 'b', 'STUD-69'),
(127, '24', '78', 'b', 'd', 'STUD-69'),
(128, '24', '79', 'd', 'd', 'STUD-69'),
(129, '24', '80', 'b', 'b', 'STUD-69'),
(130, '24', '81', 'c', 'c', 'STUD-69'),
(131, '24', '72', 'c', 'a', 'STUD-06'),
(132, '24', '73', 'b', 'c', 'STUD-06'),
(133, '24', '74', 'd', 'd', 'STUD-06'),
(134, '24', '75', 'd', 'd', 'STUD-06'),
(135, '24', '76', 'b', 'a', 'STUD-06'),
(136, '24', '77', 'b', 'b', 'STUD-06'),
(137, '24', '78', 'b', 'd', 'STUD-06'),
(138, '24', '79', 'b', 'd', 'STUD-06'),
(139, '24', '80', 'a', 'b', 'STUD-06'),
(140, '24', '81', 'c', 'c', 'STUD-06'),
(141, '25', '82', 'a', 'b', 'STUD-69'),
(142, '25', '83', 'c', 'b', 'STUD-69'),
(143, '25', '84', 'd', 'd', 'STUD-69'),
(144, '25', '85', 'c', 'c', 'STUD-69'),
(145, '25', '86', 'b', 'b', 'STUD-69'),
(146, '25', '87', 'c', 'b', 'STUD-69'),
(147, '25', '88', 'c', 'c', 'STUD-69'),
(148, '25', '89', 'b', 'b', 'STUD-69'),
(149, '25', '90', 'c', 'c', 'STUD-69'),
(150, '25', '91', 'b', 'b', 'STUD-69'),
(151, '25', '82', 'b', 'b', 'STUD-06'),
(152, '25', '83', 'b', 'b', 'STUD-06'),
(153, '25', '84', 'd', 'd', 'STUD-06'),
(154, '25', '85', 'c', 'c', 'STUD-06'),
(155, '25', '86', 'b', 'b', 'STUD-06'),
(156, '25', '87', 'a', 'b', 'STUD-06'),
(157, '25', '88', 'c', 'c', 'STUD-06'),
(158, '25', '89', 'b', 'b', 'STUD-06'),
(159, '25', '90', 'c', 'c', 'STUD-06'),
(160, '25', '91', 'b', 'b', 'STUD-06'),
(161, '26', '92', 'a', 'a', 'STUD-69'),
(162, '26', '93', 'a', 'a', 'STUD-69'),
(163, '26', '94', 'a', 'b', 'STUD-69'),
(164, '26', '95', 'd', 'd', 'STUD-69'),
(165, '26', '96', 'c', 'b', 'STUD-69'),
(166, '26', '97', 'b', 'd', 'STUD-69'),
(167, '26', '98', 'b', 'b', 'STUD-69'),
(168, '26', '99', 'd', 'd', 'STUD-69'),
(169, '26', '100', 'c', 'b', 'STUD-69'),
(170, '26', '101', 'a', 'b', 'STUD-69'),
(171, '26', '92', 'a', 'a', 'STUD-06'),
(172, '26', '93', 'a', 'a', 'STUD-06'),
(173, '26', '94', 'a', 'b', 'STUD-06'),
(174, '26', '95', 'c', 'd', 'STUD-06'),
(175, '26', '96', 'a', 'b', 'STUD-06'),
(176, '26', '97', 'c', 'd', 'STUD-06'),
(177, '26', '98', 'c', 'b', 'STUD-06'),
(178, '26', '99', 'a', 'd', 'STUD-06'),
(179, '26', '100', 'c', 'b', 'STUD-06'),
(180, '26', '101', 'c', 'b', 'STUD-06'),
(181, '27', '102', 'a', 'b', 'STUD-69'),
(182, '27', '103', 'b', 'd', 'STUD-69'),
(183, '27', '104', 'b', 'd', 'STUD-69'),
(184, '27', '105', 'c', 'c', 'STUD-69'),
(185, '27', '106', 'c', 'b', 'STUD-69'),
(186, '27', '107', 'c', 'c', 'STUD-69'),
(187, '27', '108', 'd', 'a', 'STUD-69'),
(188, '27', '109', 'a', 'a', 'STUD-69'),
(189, '27', '110', 'c', 'c', 'STUD-69'),
(190, '27', '111', 'a', 'b', 'STUD-69'),
(191, '19', '22', 'b', 'a', 'STUD-04'),
(192, '19', '23', 'a', 'a', 'STUD-04'),
(193, '19', '24', 'c', 'c', 'STUD-04'),
(194, '19', '25', 'b', 'b', 'STUD-04'),
(195, '19', '26', 'a', 'b', 'STUD-04'),
(196, '19', '27', 'b', 'd', 'STUD-04'),
(197, '19', '28', 'c', 'a', 'STUD-04'),
(198, '19', '29', 'a', 'a', 'STUD-04'),
(199, '19', '30', 'c', 'c', 'STUD-04'),
(200, '19', '31', 'c', 'a', 'STUD-04'),
(201, '19', '22', 'a', 'a', 'STUD-03'),
(202, '19', '23', 'b', 'a', 'STUD-03'),
(203, '19', '24', 'c', 'c', 'STUD-03'),
(204, '19', '25', 'd', 'b', 'STUD-03'),
(205, '19', '26', 'b', 'b', 'STUD-03'),
(206, '19', '27', 'c', 'd', 'STUD-03'),
(207, '19', '28', 'c', 'a', 'STUD-03'),
(208, '19', '29', 'd', 'a', 'STUD-03'),
(209, '19', '30', 'd', 'c', 'STUD-03'),
(210, '19', '31', 'a', 'a', 'STUD-03'),
(211, '20', '32', 'a', 'a', 'STUD-03'),
(212, '20', '33', 'a', 'd', 'STUD-03'),
(213, '20', '34', 'c', 'c', 'STUD-03'),
(214, '20', '35', 'd', 'd', 'STUD-03'),
(215, '20', '36', 'b', 'a', 'STUD-03'),
(216, '20', '37', 'b', 'd', 'STUD-03'),
(217, '20', '38', 'c', 'd', 'STUD-03'),
(218, '20', '39', 'd', 'b', 'STUD-03'),
(219, '20', '40', 'd', 'a', 'STUD-03'),
(220, '20', '41', 'b', 'c', 'STUD-03'),
(221, '21', '42', 'a', 'a', 'STUD-03'),
(222, '21', '43', 'a', 'b', 'STUD-03'),
(223, '21', '44', 'a', 'd', 'STUD-03'),
(224, '21', '45', 'a', 'c', 'STUD-03'),
(225, '21', '46', 'a', 'd', 'STUD-03'),
(226, '21', '47', 'a', 'c', 'STUD-03'),
(227, '21', '48', 'a', 'a', 'STUD-03'),
(228, '21', '49', 'a', 'a', 'STUD-03'),
(229, '21', '50', 'a', 'b', 'STUD-03'),
(230, '21', '51', 'a', 'a', 'STUD-03'),
(231, '20', '32', 'a', 'a', 'STUD-04'),
(232, '20', '33', 'c', 'd', 'STUD-04'),
(233, '20', '34', 'c', 'c', 'STUD-04'),
(234, '20', '35', 'd', 'd', 'STUD-04'),
(235, '20', '36', 'a', 'a', 'STUD-04'),
(236, '20', '37', 'd', 'd', 'STUD-04'),
(237, '20', '38', 'd', 'd', 'STUD-04'),
(238, '20', '39', 'b', 'b', 'STUD-04'),
(239, '20', '40', 'a', 'a', 'STUD-04'),
(240, '20', '41', 'c', 'c', 'STUD-04'),
(241, '22', '52', 'a', 'a', 'STUD-03'),
(242, '22', '53', 'b', 'b', 'STUD-03'),
(243, '22', '54', 'b', 'c', 'STUD-03'),
(244, '22', '55', 'b', 'c', 'STUD-03'),
(245, '22', '56', 'b', 'c', 'STUD-03'),
(246, '22', '57', 'b', 'd', 'STUD-03'),
(247, '22', '58', 'b', 'a', 'STUD-03'),
(248, '22', '59', 'b', 'b', 'STUD-03'),
(249, '22', '60', 'b', 'd', 'STUD-03'),
(250, '22', '61', 'b', 'd', 'STUD-03'),
(251, '23', '62', 'a', 'b', 'STUD-03'),
(252, '23', '63', 'a', 'c', 'STUD-03'),
(253, '23', '64', 'a', 'a', 'STUD-03'),
(254, '23', '65', 'c', 'd', 'STUD-03'),
(255, '23', '66', 'c', 'b', 'STUD-03'),
(256, '23', '67', 'a', 'a', 'STUD-03'),
(257, '23', '68', 'c', 'b', 'STUD-03'),
(258, '23', '69', 'c', 'a', 'STUD-03'),
(259, '23', '70', 'd', 'd', 'STUD-03'),
(260, '23', '71', 'c', 'c', 'STUD-03'),
(261, '22', '52', 'a', 'a', 'STUD-04'),
(262, '22', '53', 'b', 'b', 'STUD-04'),
(263, '22', '54', 'c', 'c', 'STUD-04'),
(264, '22', '55', 'c', 'c', 'STUD-04'),
(265, '22', '56', 'c', 'c', 'STUD-04'),
(266, '22', '57', 'd', 'd', 'STUD-04'),
(267, '22', '58', 'a', 'a', 'STUD-04'),
(268, '22', '59', 'b', 'b', 'STUD-04'),
(269, '22', '60', 'd', 'd', 'STUD-04'),
(270, '22', '61', 'd', 'd', 'STUD-04'),
(271, '24', '72', 'a', 'a', 'STUD-03'),
(272, '24', '73', 'a', 'c', 'STUD-03'),
(273, '24', '74', 'a', 'd', 'STUD-03'),
(274, '24', '75', 'a', 'd', 'STUD-03'),
(275, '24', '76', 'a', 'a', 'STUD-03'),
(276, '24', '77', 'a', 'b', 'STUD-03'),
(277, '24', '78', 'a', 'd', 'STUD-03'),
(278, '24', '79', 'a', 'd', 'STUD-03'),
(279, '24', '80', 'a', 'b', 'STUD-03'),
(280, '24', '81', 'a', 'c', 'STUD-03'),
(281, '25', '82', 'a', 'b', 'STUD-03'),
(282, '25', '83', 'b', 'b', 'STUD-03'),
(283, '25', '84', 'd', 'd', 'STUD-03'),
(284, '25', '85', 'c', 'c', 'STUD-03'),
(285, '25', '86', 'b', 'b', 'STUD-03'),
(286, '25', '87', 'a', 'b', 'STUD-03'),
(287, '25', '88', 'c', 'c', 'STUD-03'),
(288, '25', '89', 'a', 'b', 'STUD-03'),
(289, '25', '90', 'c', 'c', 'STUD-03'),
(290, '25', '91', 'b', 'b', 'STUD-03'),
(291, '26', '92', 'a', 'a', 'STUD-03'),
(292, '26', '93', 'a', 'a', 'STUD-03'),
(293, '26', '94', 'c', 'b', 'STUD-03'),
(294, '26', '95', 'a', 'd', 'STUD-03'),
(295, '26', '96', 'c', 'b', 'STUD-03'),
(296, '26', '97', 'b', 'd', 'STUD-03'),
(297, '26', '98', 'a', 'b', 'STUD-03'),
(298, '26', '99', 'c', 'd', 'STUD-03'),
(299, '26', '100', 'a', 'b', 'STUD-03'),
(300, '26', '101', 'c', 'b', 'STUD-03'),
(301, '27', '102', 'a', 'b', 'STUD-03'),
(302, '27', '103', 'b', 'd', 'STUD-03'),
(303, '27', '104', 'c', 'd', 'STUD-03'),
(304, '27', '105', 'd', 'c', 'STUD-03'),
(305, '27', '106', 'a', 'b', 'STUD-03'),
(306, '27', '107', 'b', 'c', 'STUD-03'),
(307, '27', '108', 'c', 'a', 'STUD-03'),
(308, '27', '109', 'c', 'a', 'STUD-03'),
(309, '27', '110', 'a', 'c', 'STUD-03'),
(310, '27', '111', 'd', 'b', 'STUD-03'),
(311, '19', '22', 'b', 'a', 'STUD-13'),
(312, '19', '23', 'a', 'a', 'STUD-13'),
(313, '19', '24', 'c', 'c', 'STUD-13'),
(314, '19', '25', 'd', 'b', 'STUD-13'),
(315, '19', '26', 'b', 'b', 'STUD-13'),
(316, '19', '27', 'c', 'd', 'STUD-13'),
(317, '19', '28', 'b', 'a', 'STUD-13'),
(318, '19', '29', 'a', 'a', 'STUD-13'),
(319, '19', '30', 'c', 'c', 'STUD-13'),
(320, '19', '31', 'b', 'a', 'STUD-13'),
(321, '20', '32', 'b', 'a', 'STUD-13'),
(322, '20', '33', 'a', 'd', 'STUD-13'),
(323, '20', '34', 'c', 'c', 'STUD-13'),
(324, '20', '35', 'd', 'd', 'STUD-13'),
(325, '20', '36', 'b', 'a', 'STUD-13'),
(326, '20', '37', 'a', 'd', 'STUD-13'),
(327, '20', '38', 'c', 'd', 'STUD-13'),
(328, '20', '39', 'd', 'b', 'STUD-13'),
(329, '20', '40', 'a', 'a', 'STUD-13'),
(330, '20', '41', 'd', 'c', 'STUD-13'),
(331, '23', '62', 'b', 'b', 'STUD-04'),
(332, '23', '63', 'c', 'c', 'STUD-04'),
(333, '23', '64', 'a', 'a', 'STUD-04'),
(334, '23', '65', 'd', 'd', 'STUD-04'),
(335, '23', '66', 'b', 'b', 'STUD-04'),
(336, '23', '67', 'a', 'a', 'STUD-04'),
(337, '23', '68', 'b', 'b', 'STUD-04'),
(338, '23', '69', 'a', 'a', 'STUD-04'),
(339, '23', '70', 'd', 'd', 'STUD-04'),
(340, '23', '71', 'c', 'c', 'STUD-04'),
(341, '22', '52', 'a', 'a', 'STUD-13'),
(342, '22', '53', 'b', 'b', 'STUD-13'),
(343, '22', '54', 'c', 'c', 'STUD-13'),
(344, '22', '55', 'a', 'c', 'STUD-13'),
(345, '22', '56', 'b', 'c', 'STUD-13'),
(346, '22', '57', 'c', 'd', 'STUD-13'),
(347, '22', '58', 'd', 'a', 'STUD-13'),
(348, '22', '59', 'a', 'b', 'STUD-13'),
(349, '22', '60', 'c', 'd', 'STUD-13'),
(350, '22', '61', 'b', 'd', 'STUD-13'),
(351, '23', '62', 'b', 'b', 'STUD-13'),
(352, '23', '63', 'a', 'c', 'STUD-13'),
(353, '23', '64', 'c', 'a', 'STUD-13'),
(354, '23', '65', 'b', 'd', 'STUD-13'),
(355, '23', '66', 'b', 'b', 'STUD-13'),
(356, '23', '67', 'd', 'a', 'STUD-13'),
(357, '23', '68', 'b', 'b', 'STUD-13'),
(358, '23', '69', 'b', 'a', 'STUD-13'),
(359, '23', '70', 'd', 'd', 'STUD-13'),
(360, '23', '71', 'c', 'c', 'STUD-13'),
(361, '21', '42', 'a', 'a', 'STUD-04'),
(362, '21', '43', 'b', 'b', 'STUD-04'),
(363, '21', '44', 'd', 'd', 'STUD-04'),
(364, '21', '45', 'c', 'c', 'STUD-04'),
(365, '21', '46', 'd', 'd', 'STUD-04'),
(366, '21', '47', 'c', 'c', 'STUD-04'),
(367, '21', '48', 'a', 'a', 'STUD-04'),
(368, '21', '49', 'a', 'a', 'STUD-04'),
(369, '21', '50', 'b', 'b', 'STUD-04'),
(370, '21', '51', 'a', 'a', 'STUD-04'),
(371, '19', '22', 'b', 'a', 'STUD-09'),
(372, '19', '23', 'a', 'a', 'STUD-09'),
(373, '19', '24', 'c', 'c', 'STUD-09'),
(374, '19', '25', 'b', 'b', 'STUD-09'),
(375, '19', '26', 'a', 'b', 'STUD-09'),
(376, '19', '27', 'a', 'd', 'STUD-09'),
(377, '19', '28', 'a', 'a', 'STUD-09'),
(378, '19', '29', 'a', 'a', 'STUD-09'),
(379, '19', '30', 'c', 'c', 'STUD-09'),
(380, '19', '31', 'a', 'a', 'STUD-09'),
(381, '24', '72', 'a', 'a', 'STUD-13'),
(382, '24', '73', 'b', 'c', 'STUD-13'),
(383, '24', '74', 'd', 'd', 'STUD-13'),
(384, '24', '75', 'b', 'd', 'STUD-13'),
(385, '24', '76', 'b', 'a', 'STUD-13'),
(386, '24', '77', 'b', 'b', 'STUD-13'),
(387, '24', '78', 'b', 'd', 'STUD-13'),
(388, '24', '79', 'a', 'd', 'STUD-13'),
(389, '24', '80', 'b', 'b', 'STUD-13'),
(390, '24', '81', 'b', 'c', 'STUD-13'),
(391, '24', '72', 'a', 'a', 'STUD-04'),
(392, '24', '73', 'c', 'c', 'STUD-04'),
(393, '24', '74', 'd', 'd', 'STUD-04'),
(394, '24', '75', 'd', 'd', 'STUD-04'),
(395, '24', '76', 'a', 'a', 'STUD-04'),
(396, '24', '77', 'b', 'b', 'STUD-04'),
(397, '24', '78', 'd', 'd', 'STUD-04'),
(398, '24', '79', 'd', 'd', 'STUD-04'),
(399, '24', '80', 'b', 'b', 'STUD-04'),
(400, '24', '81', 'c', 'c', 'STUD-04'),
(401, '25', '82', 'b', 'b', 'STUD-13'),
(402, '25', '83', 'a', 'b', 'STUD-13'),
(403, '25', '84', 'd', 'd', 'STUD-13'),
(404, '25', '85', 'c', 'c', 'STUD-13'),
(405, '25', '86', 'b', 'b', 'STUD-13'),
(406, '25', '87', 'a', 'b', 'STUD-13'),
(407, '25', '88', 'a', 'c', 'STUD-13'),
(408, '25', '89', 'd', 'b', 'STUD-13'),
(409, '25', '90', 'c', 'c', 'STUD-13'),
(410, '25', '91', 'a', 'b', 'STUD-13'),
(411, '21', '42', 'c', 'a', 'STUD-13'),
(412, '21', '43', 'a', 'b', 'STUD-13'),
(413, '21', '44', 'a', 'd', 'STUD-13'),
(414, '21', '45', 'b', 'c', 'STUD-13'),
(415, '21', '46', 'b', 'd', 'STUD-13'),
(416, '21', '47', 'c', 'c', 'STUD-13'),
(417, '21', '48', 'a', 'a', 'STUD-13'),
(418, '21', '49', 'b', 'a', 'STUD-13'),
(419, '21', '50', 'a', 'b', 'STUD-13'),
(420, '21', '51', 'b', 'a', 'STUD-13'),
(421, '20', '32', 'a', 'a', 'STUD-09'),
(422, '20', '33', 'b', 'd', 'STUD-09'),
(423, '20', '34', 'c', 'c', 'STUD-09'),
(424, '20', '35', 'a', 'd', 'STUD-09'),
(425, '20', '36', 'a', 'a', 'STUD-09'),
(426, '20', '37', 'd', 'd', 'STUD-09'),
(427, '20', '38', 'a', 'd', 'STUD-09'),
(428, '20', '39', 'c', 'b', 'STUD-09'),
(429, '20', '40', 'a', 'a', 'STUD-09'),
(430, '20', '41', 'a', 'c', 'STUD-09'),
(431, '25', '82', 'b', 'b', 'STUD-04'),
(432, '25', '83', 'b', 'b', 'STUD-04'),
(433, '25', '84', 'd', 'd', 'STUD-04'),
(434, '25', '85', 'c', 'c', 'STUD-04'),
(435, '25', '86', 'b', 'b', 'STUD-04'),
(436, '25', '87', 'b', 'b', 'STUD-04'),
(437, '25', '88', 'c', 'c', 'STUD-04'),
(438, '25', '89', 'a', 'b', 'STUD-04'),
(439, '25', '90', 'c', 'c', 'STUD-04'),
(440, '25', '91', 'b', 'b', 'STUD-04'),
(441, '26', '92', 'a', 'a', 'STUD-13'),
(442, '26', '93', 'a', 'a', 'STUD-13'),
(443, '26', '94', 'a', 'b', 'STUD-13'),
(444, '26', '95', 'a', 'd', 'STUD-13'),
(445, '26', '96', 'a', 'b', 'STUD-13'),
(446, '26', '97', 'a', 'd', 'STUD-13'),
(447, '26', '98', 'a', 'b', 'STUD-13'),
(448, '26', '99', 'a', 'd', 'STUD-13'),
(449, '26', '100', 'a', 'b', 'STUD-13'),
(450, '26', '101', 'a', 'b', 'STUD-13'),
(451, '27', '102', 'b', 'b', 'STUD-13'),
(452, '27', '103', 'b', 'd', 'STUD-13'),
(453, '27', '104', 'b', 'd', 'STUD-13'),
(454, '27', '105', 'b', 'c', 'STUD-13'),
(455, '27', '106', 'b', 'b', 'STUD-13'),
(456, '27', '107', 'b', 'c', 'STUD-13'),
(457, '27', '108', 'b', 'a', 'STUD-13'),
(458, '27', '109', 'b', 'a', 'STUD-13'),
(459, '27', '110', 'b', 'c', 'STUD-13'),
(460, '27', '111', 'b', 'b', 'STUD-13'),
(461, '22', '52', 'a', 'a', 'STUD-09'),
(462, '22', '53', 'a', 'b', 'STUD-09'),
(463, '22', '54', 'c', 'c', 'STUD-09'),
(464, '22', '55', 'a', 'c', 'STUD-09'),
(465, '22', '56', 'c', 'c', 'STUD-09'),
(466, '22', '57', 'a', 'd', 'STUD-09'),
(467, '22', '58', 'b', 'a', 'STUD-09'),
(468, '22', '59', 'c', 'b', 'STUD-09'),
(469, '22', '60', 'a', 'd', 'STUD-09'),
(470, '22', '61', 'd', 'd', 'STUD-09'),
(471, '26', '92', 'a', 'a', 'STUD-04'),
(472, '26', '93', 'a', 'a', 'STUD-04'),
(473, '26', '94', 'b', 'b', 'STUD-04'),
(474, '26', '95', 'd', 'd', 'STUD-04'),
(475, '26', '96', 'b', 'b', 'STUD-04'),
(476, '26', '97', 'd', 'd', 'STUD-04'),
(477, '26', '98', 'b', 'b', 'STUD-04'),
(478, '26', '99', 'd', 'd', 'STUD-04'),
(479, '26', '100', 'b', 'b', 'STUD-04'),
(480, '26', '101', 'b', 'b', 'STUD-04'),
(481, '27', '102', 'b', 'b', 'STUD-04'),
(482, '27', '103', 'd', 'd', 'STUD-04'),
(483, '27', '104', 'c', 'd', 'STUD-04'),
(484, '27', '105', 'c', 'c', 'STUD-04'),
(485, '27', '106', 'b', 'b', 'STUD-04'),
(486, '27', '107', 'c', 'c', 'STUD-04'),
(487, '27', '108', 'a', 'a', 'STUD-04'),
(488, '27', '109', 'a', 'a', 'STUD-04'),
(489, '27', '110', 'c', 'c', 'STUD-04'),
(490, '27', '111', 'b', 'b', 'STUD-04'),
(491, '23', '62', 'b', 'b', 'STUD-09'),
(492, '23', '63', 'c', 'c', 'STUD-09'),
(493, '23', '64', 'a', 'a', 'STUD-09'),
(494, '23', '65', 'c', 'd', 'STUD-09'),
(495, '23', '66', 'c', 'b', 'STUD-09'),
(496, '23', '67', 'a', 'a', 'STUD-09'),
(497, '23', '68', 'c', 'b', 'STUD-09'),
(498, '23', '69', 'b', 'a', 'STUD-09'),
(499, '23', '70', 'd', 'd', 'STUD-09'),
(500, '23', '71', 'c', 'c', 'STUD-09'),
(501, '24', '72', 'b', 'a', 'STUD-09'),
(502, '24', '73', 'c', 'c', 'STUD-09'),
(503, '24', '74', 'd', 'd', 'STUD-09'),
(504, '24', '75', 'd', 'd', 'STUD-09'),
(505, '24', '76', 'a', 'a', 'STUD-09'),
(506, '24', '77', 'b', 'b', 'STUD-09'),
(507, '24', '78', 'd', 'd', 'STUD-09'),
(508, '24', '79', 'b', 'd', 'STUD-09'),
(509, '24', '80', 'b', 'b', 'STUD-09'),
(510, '24', '81', 'c', 'c', 'STUD-09'),
(511, '25', '82', 'a', 'b', 'STUD-09'),
(512, '25', '83', 'd', 'b', 'STUD-09'),
(513, '25', '84', 'd', 'd', 'STUD-09'),
(514, '25', '85', 'c', 'c', 'STUD-09'),
(515, '25', '86', 'b', 'b', 'STUD-09'),
(516, '25', '87', 'b', 'b', 'STUD-09'),
(517, '25', '88', 'c', 'c', 'STUD-09'),
(518, '25', '89', 'a', 'b', 'STUD-09'),
(519, '25', '90', 'd', 'c', 'STUD-09'),
(520, '25', '91', 'b', 'b', 'STUD-09'),
(521, '21', '42', 'a', 'a', 'STUD-09'),
(522, '21', '43', 'b', 'b', 'STUD-09'),
(523, '21', '44', 'd', 'd', 'STUD-09'),
(524, '21', '45', 'c', 'c', 'STUD-09'),
(525, '21', '46', 'd', 'd', 'STUD-09'),
(526, '21', '47', 'a', 'c', 'STUD-09'),
(527, '21', '48', 'a', 'a', 'STUD-09'),
(528, '21', '49', 'a', 'a', 'STUD-09'),
(529, '21', '50', 'a', 'b', 'STUD-09'),
(530, '21', '51', 'a', 'a', 'STUD-09'),
(531, '26', '92', 'a', 'a', 'STUD-09'),
(532, '26', '93', 'a', 'a', 'STUD-09'),
(533, '26', '94', 'a', 'b', 'STUD-09'),
(534, '26', '95', 'a', 'd', 'STUD-09'),
(535, '26', '96', 'b', 'b', 'STUD-09'),
(536, '26', '97', 'd', 'd', 'STUD-09'),
(537, '26', '98', 'a', 'b', 'STUD-09'),
(538, '26', '99', 'a', 'd', 'STUD-09'),
(539, '26', '100', 'b', 'b', 'STUD-09'),
(540, '26', '101', 'b', 'b', 'STUD-09'),
(541, '27', '102', 'a', 'b', 'STUD-09'),
(542, '27', '103', 'd', 'd', 'STUD-09'),
(543, '27', '104', 'c', 'd', 'STUD-09'),
(544, '27', '105', 'c', 'c', 'STUD-09'),
(545, '27', '106', 'b', 'b', 'STUD-09'),
(546, '27', '107', 'c', 'c', 'STUD-09'),
(547, '27', '108', 'c', 'a', 'STUD-09'),
(548, '27', '109', 'a', 'a', 'STUD-09'),
(549, '27', '110', 'c', 'c', 'STUD-09'),
(550, '27', '111', 'b', 'b', 'STUD-09'),
(551, '19', '22', 'c', 'a', '12'),
(552, '19', '23', 'a', 'a', '12'),
(553, '19', '24', 'd', 'c', '12'),
(554, '19', '25', 'b', 'b', '12'),
(555, '19', '26', 'a', 'b', '12'),
(556, '19', '27', 'a', 'd', '12'),
(557, '19', '28', 'b', 'a', '12'),
(558, '19', '29', 'c', 'a', '12'),
(559, '19', '30', 'a', 'c', '12'),
(560, '19', '31', 'a', 'a', '12'),
(561, '19', '22', 'b', 'a', '1600413900'),
(562, '19', '23', 'a', 'a', '1600413900'),
(563, '19', '24', 'c', 'c', '1600413900'),
(564, '19', '25', 'c', 'b', '1600413900'),
(565, '19', '26', 'b', 'b', '1600413900'),
(566, '19', '27', 'd', 'd', '1600413900'),
(567, '19', '28', 'a', 'a', '1600413900'),
(568, '19', '29', 'c', 'a', '1600413900'),
(569, '19', '30', 'c', 'c', '1600413900'),
(570, '19', '31', 'a', 'a', '1600413900'),
(571, '19', '22', 'b', 'a', '1600413800'),
(572, '19', '23', 'a', 'a', '1600413800'),
(573, '19', '24', 'c', 'c', '1600413800'),
(574, '19', '25', 'd', 'b', '1600413800'),
(575, '19', '26', 'a', 'b', '1600413800'),
(576, '19', '27', 'b', 'd', '1600413800'),
(577, '19', '28', 'a', 'a', '1600413800'),
(578, '19', '29', 'a', 'a', '1600413800'),
(579, '19', '30', 'c', 'c', '1600413800'),
(580, '19', '31', 'a', 'a', '1600413800');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user` text NOT NULL,
  `pass` text NOT NULL,
  `access` text NOT NULL,
  `student_id` text NOT NULL,
  `name` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `user`, `pass`, `access`, `student_id`, `name`, `status`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'developer', '', 'superadmin', 'offline'),
(2, 'student', 'cd73502828457d15655bbd7a63fb0bc8', 'student', 'STUD-69', 'Roben Summer', 'offline'),
(4, 'Robie', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-01', 'Robie Therese Guaring', 'online'),
(7, 'james', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-04', 'James Baptist', 'offline'),
(8, 'Mat', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-05', 'Matthew of Galilee', 'offline'),
(9, 'peter', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-06', 'Peter Shimon', 'offline'),
(10, 'andrew', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-07', 'Andrew Shimon', 'online'),
(11, 'James1', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-08', 'James the Great', 'offline'),
(12, 'john', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-09', ' John the Apostle', 'online'),
(13, 'philip', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-10', 'Philip the Apostle', 'offline'),
(14, 'barth', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-11', 'Bartholomew of Iudaea', 'offline'),
(15, 'thomas', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'Stud-12', 'Judas Thomas', 'offline'),
(16, 'bossg', '81dc9bdb52d04dc20036dbd8313ed055', 'student', 'STUD-13', 'Glenn the Baptist', 'online'),
(19, 'abby', 'dddf13bb42451ae0d62bf26c65899873', 'student', '12', 'abigail tugaoen', 'offline'),
(26, 'Prof', '7ecc19e1a0be36ba2c6f05d06b5d3058', 'admin', '', 'Proffesor X', 'online');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_exam_item`
--
ALTER TABLE `tbl_exam_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_lecture`
--
ALTER TABLE `tbl_lecture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student_answer`
--
ALTER TABLE `tbl_student_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_exam_item`
--
ALTER TABLE `tbl_exam_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `tbl_lecture`
--
ALTER TABLE `tbl_lecture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_student_answer`
--
ALTER TABLE `tbl_student_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=581;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
