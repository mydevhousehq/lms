# LMS Night's Watch 

how to use
```
referrence: https://gitlab.com/czar.gaba/self-class-library for local development

for mobile:
cd courseware-mobilepatch
npm i
```


<h2>Web App / API</h2>

```
- lecture management
- API ( currently set to https://nstp.mydevhouse.dev/api )
- mobile app ( note: android 7+ only )
- Student management

```

<h2> Mobile App </h2>

```
- can view lectures 
- can view students and status
- dynamic data from api/web-app
```

<h2>Pending</h2>

```
- profile management
- quiz management
- grading calculation
- Generate Report EZ
```


<h3>Developer's Recommendation ( myself )</h3>

```
- needs chatting system
- onscreen view pdf on mobile
- onscreen view youtube embeded on mobile 
- exam notifier and deadline
- whatever you think contact us info@mydevhouse.dev
```

<h5>Authors </h5>

```
XIV Lord Winter | NW | IT | WTF
Prince Summer | NW | WTF
```
