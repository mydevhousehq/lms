<?php include('header.php');?>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">
<?php 
if($access=="student"){
  $display="none";
}
?>
  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid print">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Grade</h1>
<div class="row" style="display:<?php echo $display;?>">
      <!-- admin view  -->
      <?php 
$data = custom_query("SELECT * FROM `tbl_user` where `access`='student'");
foreach ($data as $row) {
  $student_id=$row['student_id'];
  ?>
    <div class="col-md-4" style="margin-top:10px;">
    <div class="card bg-light text-black shadow">

    <div class="card-header">
    <?php echo $row['name'];?>
                </div>

      <div class="card-body">
       
        <table  class="table">
        <?php 
$datax = custom_query("SELECT * FROM `tbl_exam`  group by type asc");
foreach ($datax as $rowx) {
  $qid=$rowx['id'];
  $type=$rowx['type'];
  $dataxx = custom_query("select count(*) as count FROM `tbl_student_answer` WHERE `student_answer`=`correct_answer` and `student_id`='$student_id' and `qid`='$qid'");
  foreach ($dataxx as $rowxx) {
      // echo $rowx['id']."<br />\n";
      $score=$rowxx['count'];
  }
        ?>
        <?php 
        // custom query usage
        $dataxx = custom_query("select count(*) as count FROM `tbl_exam_item` WHERE `qid`='$qid'");
        foreach ($dataxx as $rowxx) {
            // echo $rowx['id']."<br />\n";
            $items=$rowxx['count'];
        }
        ?>
        <tr>
          <td><?php echo $rowx['title']." | ".$rowx['type'];?></td>
          <td><?php echo $score;?> /  <?php echo $items;?></td>
        </tr>
        <?php 

        $pp=  percentage($score,$items);
        $arr[$type]=$pp;
}
        ?>
        <!-- <div class="text-black-50 small">#f8f9fc</div> -->
        <tr>
          <td>Final Grade:</td>
          <td><?php echo gradedata($arr);?></td>
        </tr>
        </table>
      </div>
    </div>
    </div>

  <?php 

}

      ?>
</div>

      <!-- student section view  -->

      <?php if($access=="student"){
       // $arr = array();
?>
      <div class="row">
        
        <div class="col-md-12">
          <h3>Student: <?php echo $name;?></h3>
        </div>
        <?php 
        // GET usage
        $data = custom_query("SELECT * FROM `tbl_exam`  group by type asc");
        foreach ($data as $row) {
            // echo $row['name']."<br />\n";
            $type=$row['type'];
            $qid=$row['id'];
            ?>
              <div class="col-md-4" style="margin-top:10px;">

              <div class="card bg-light text-black shadow">
                    <div class="card-body">
                     <!-- body  -->
                     <h2><?php echo $row['title'];?></h2>
                      <h5><?php echo $row['type'];?></h5>
                      <?php 
                        // custom query usage
                        $datax = custom_query("select count(*) as count FROM `tbl_student_answer` WHERE `student_answer`=`correct_answer` and `student_id`='$student_id' and `qid`='$qid'");
                        foreach ($datax as $rowx) {
                            // echo $rowx['id']."<br />\n";
                            $score=$rowx['count'];
                        }
                        ?>

                        <?php 
                        // custom query usage
                        $dataxx = custom_query("select count(*) as count FROM `tbl_exam_item` WHERE `qid`='$qid'");
                        foreach ($dataxx as $rowxx) {
                            // echo $rowx['id']."<br />\n";
                            $items=$rowxx['count'];
                        }
                        ?>
                        score: <?php echo $score;?> /  <?php echo $items;?><br>

                        <?php 
                         $pp=  percentage($score,$items);
                         $arr[$type]=$pp;
                        // array_push($arr, [$type=>$pp]);
                         
                        //  echo $g_prelim;
                        ?>
                     <!-- end cbody -->
                    </div>
              </div>
              
            
              </div>
              
            <?php 
           
        }
        ?>

         <!-- grade section  -->
         <div class="col-md-4" style="margin-top:10px;">

<div class="card bg-light text-black shadow">
      <div class="card-body">
      <h2>Final Grade</h2>
      <h4><?php echo gradedata($arr);?></h4>
      </div>
</div>


</div>
         <!-- end grade section  -->
      </div>
<?php 
    // print_r($arr);
   // echo $arr['Q1'];  //
    //echo gradedata($arr);
    }?>
     
             

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
<style>
  @media print{
     ul#accordionSidebar{display:none !important;}
     
    .print , .print * {
     
    }
  }

  .card-header{background:#000;color:#fff;}
</style>
  <?php include('footer.php');?>