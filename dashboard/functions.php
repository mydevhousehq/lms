<?php
//for functions

// auto import php files on includes folder
foreach (glob("../includes/*.php") as $filename){
    include $filename;
}

date_default_timezone_set('Asia/Manila');//time zone
   //get connection
   $conn = getConnection();

   //declaration
   $path = "../api/uploads/";

   // user detail fetch
   $username = $_SESSION['user'];
    $data = custom_query("SELECT * FROM `tbl_user` where user='$username'");
    foreach ($data as $row) {
        // echo $row['id']."<br />\n";
        $name = $row['name'];
        $userid=$row['id'];
        $student_id=$row['student_id'];
        $access=$row['access'];
    }
   

    function percentage( $number, $total, $decimals = 2 ){
        return round( $number / $total * 100, $decimals );
    }

    function gradedata($arr){
        

        $g_prelim = ($arr['Q1'] * .2) +  ($arr['Q2'] * .2 ) + ($arr['prelim'] *.6 );
        $g_midterm = ($arr['Q3'] * .2) +  ($arr['Q4'] * .2 ) + ($arr['midterm'] *.6 );
        $g_final = ($arr['Q5'] * .2) +  ($arr['Q6'] * .2 ) + ($arr['finals'] *.6 );
        $final_grade =  ($g_prelim * .3 ) + ($g_midterm * .3 ) + ( $g_final * .4);
        return $final_grade;
    }

    