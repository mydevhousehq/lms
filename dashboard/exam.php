<?php include('header.php');?>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">
        <?php if($access=="admin"){echo "EXAM MANAGEMENT";}else{echo "EXAM PAGE";}?>
      </h1>
      <!-- admin page  -->
      <?php if($access=="admin"){
        ?>
          <div class="row">
            <div class="col-md-4">
              <!-- first col  -->
              <div class="card mb-4">
                <div class="card-header">
                  Add Exam
                </div>
                <div class="card-body">
                  <form action="process.php" method="post">
                    <input type="hidden" name="return" value="<?php fileclass();?>">
                    <input type="hidden" name="process" value="add_exam">
                    <label for="">Title</label>
                    <input type="text" name="title" required class="form-control">
                    <label for="">Exam Type</label>
                    <select name="type" id="" required class="form-control">
                      <option value="Q1">Quiz 1</option>
                      <option value="Q2">Quiz 2</option>
                      <option value="Q3">Quiz 3</option>
                      <option value="Q4">Quiz 4</option>
                      <option value="Q5">Quiz 5</option>
                      <option value="Q6">Quiz 6</option>
                      <option value="prelim">Prelim</option>
                      <option value="midterm">Midterm</option>
                      <option value="finals">Finals</option>
                    </select>
                    <input type="submit" value="submit" class="btn btn-lg btn-success" style="margin-top:20px;">
                  </form>
                </div>
              </div>
              <!-- end col  -->
            </div>
            <div class="col-md-4">
              <!-- semi process  -->
              <?php 
                if(isset($_GET['process'])){
                  if($_GET['process']=="remove"){
                    // echo "remove";
                    if(delete($_GET['id'],'tbl_exam')){
                      ?>
                      <div class="card bg-danger text-white shadow">
                        <div class="card-body">
                          Deleted <?php echo $_GET['title'];?>
                          <!-- <div class="text-white-50 small">#e74a3b</div> -->
                        </div>
                      </div>
                      <?php 
                    }else{
                    	echo 'echo on query';
                    }
                    //
                  }
                }
              ?>
              <!-- end semi process  -->
              <!-- exam -->
              <?php 
              $data = custom_query("SELECT * FROM `tbl_exam`  group by type asc ");
              foreach ($data as $row) {
                  ?>
                  <div class="card mb-4">
                    <div class="card-header">
                      <?php echo $row['title'];?>
                    </div>
                    <div class="card-body">
                      <a href="?id=<?php echo $row['id'];?>&type=<?php echo $row['type'];?>&view=itemview&title=<?php echo $row['title'];?>" class="btn btn-info">View ITEMS</a>
                      <!-- <a href="?id=<?php echo $row['id'];?>" class="btn btn-info">Add ITEM</a> -->
                      <a href="?id=<?php echo $row['id'];?>&process=remove&title=<?php echo $row['title'];?>" class="btn btn-danger">Remove Exam</a>
                    </div>
                  </div>
                  <?php 
              }
              ?>
              <!-- exam  -->
            </div>
            <div class="col-md-4">
            
              <!-- view test and items -->
              <?php 
              if(isset($_GET['view'])){
                 ?>
                 <?php 
                 if(isset($_GET['removeid'])){
                   $rid=$_GET['removeid'];
                    if(delete($rid,'tbl_exam_item')){
                      ?>
                      <script>alert('Item Removed');</script>
                      <?php 
                    }else{
                    	echo 'echo on query';
                    }
                 }
                 ?>
                 <div class="card bg-light text-black shadow">
                    <div class="card-body">
                      <h3><?php echo $_GET['title'];?></h3>
                      <!-- <div class="text-black-50 small">#f8f9fc</div> -->
                      <!-- item list -->
                      <?php 
                      $id = $_GET['id'];
                      $data = custom_query("select * from tbl_exam_item where qid='$id'");
                      foreach ($data as $row) {
                          // echo $row['id']."<br />\n";
                          ?>
                          <div class="card bg-light text-black shadow" style="margin-bottom:10px;">
                            <div class="card-body">
                                Question: <br>
                                <?php echo $row['question'];?>
                                <br>
                                A: <?php echo $row['a'];?><br>
                                B: <?php echo $row['b'];?><br>
                                C: <?php echo $row['c'];?><br>
                                D: <?php echo $row['d'];?><br>
                                <br>
                                Correct Answer: <?php echo $row['correct'];?>
                                <br>
                                <a href="?id=<?php echo $_GET['id'];?>&view=itemview&title=<?php echo $_GET['title'];?>&removeid=<?php echo $row['id'];?>">Remove Question</a>
                            </div>
                          </div>
                          <?php 
                      }
                      ?>
                      <!-- end item list  -->

                 
                      <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter<?php echo $_GET['id'];?>">
  Add ITEM
</button>

                      <!--  form  -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter<?php echo $_GET['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ADD QUESTION</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="process.php" method="post">
      <div class="modal-body">
                        <input type="hidden" name="return" value="<?php fileclass();?>">
                        <input type="hidden" name="process" value="add_item">
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                        <input type="hidden" name="type" value="<?php echo $_GET['type'];?>">
                        <input type="hidden" name="title" value="<?php echo $_GET['title'];?>">
                        <input type="hidden" name="view" value="<?php echo $_GET['view'];?>">
                        <label for="question"> Question</label>
                        <textarea  id="" cols="30" rows="10" name="question" class="form-control"></textarea>
                        <label for="">A</label>
                        <input type="text" name="a" class="form-control" required>
                        <label for="">B</label>
                        <input type="text" name="b" class="form-control" required>
                        <label for="">C</label>
                        <input type="text" name="c" class="form-control" required>
                        <label for="">D</label>
                        <input type="text" name="d" class="form-control" required>
                        <label for="">Correct Answer</label>
                        <select name="correct" required id="" class="form-control">
                          <option value="a">A</option>
                          <option value="b">B</option>
                          <option value="c">C</option>
                          <option value="d">D</option>
                        </select>
                        <!-- <input type="submit" value="submit" class="btn btn-success" style="margin-top:20px"> -->
                      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
                      <!-- end after form  -->

                    </div>
                  </div>
                 <?php 
              }
              ?>
              <!-- last col md 4  -->
            </div>
			<!-- end row admin -->
          </div>
        <?php
      }?>

      
      <!-- student page  -->
      <?php if($access=="student"){
        
      }?>


    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

  <?php include('footer.php');?>