<?php include('header.php');?>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Student Management</h1>

        <div class="row">

            <div class="col-md-6">
            <!-- add student  -->
            
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Enroll Students</h6>
                  <div class="dropdown no-arrow">
                    <!-- <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a> -->
                    <!-- <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      
                    </div> -->
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  
                <form action="process.php" method="post">
                <input type="hidden" name="process" value="add">
                <input type="hidden" name="return" value="<?php fileclass();?>">
                <div class="form-group">
                    <label for="pwd">student id:</label>
                    <input type="text" class="form-control" name="sid"required>
                </div>

                <div class="form-group">
                    <label for="">username:</label>
                    <input type="text" class="form-control" name="user" required>
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" name="pass" required>
                </div>

                <div class="form-group">
                    <label for="pwd">name:</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                
                <button type="submit" class="btn btn-info">Submit</button>
                </form>
               


                </div>
              </div>
            <!-- end add  -->
            </div>

            <div class="col-md-6">
            
                <!-- student list  -->

                <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Student List</h6>
                  <div class="dropdown no-arrow">
                    <!-- <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a> -->
                    <!-- <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      
                    </div> -->
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  



                <table class="table">
                    <thead>
                        <tr>
                        
                        <th scope="col">student id</th>
                        <th scope="col">name</th>
                        <th scope="col">status</th>
                        <th scope="col">options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- data  -->
                        <?php 
                        // get where field
                        $data = get_where_fieldvalue('tbl_user','access','student');
                        foreach ($data as $row) {
                            
                            //online first view
                            if($row['status']=="online"){
                                ?>
                                <tr>
                                    <td><?php echo $row['student_id'];?></td>
                                    <td><?php echo $row['name'];?></td>
                                    <td><?php echo $row['status'];?></td>
                                    <td>
                                    <form action="process.php" method="post">
                                            <input type="hidden" name="return" value="<?php fileclass();?>">
                                            <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                                            <input type="hidden" name="process" value="update">
                                            <input type="submit" value="reset password" class="btn btn-warning">
                                        </form>
                                        <form action="process.php" method="post">
                                            <input type="hidden" name="return" value="<?php fileclass();?>">
                                            <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                                            <input type="hidden" name="process" value="delete">
                                            <input type="submit" value="remove student" class="btn btn-danger">
                                        </form>
    
                                        
                                    </td>
                                </tr>
                                <?php 
                            }else{
                                ?>
                            <tr>
                                <td><?php echo $row['student_id'];?></td>
                                <td><?php echo $row['name'];?></td>
                                <td><?php echo $row['status'];?></td>
                                <td>
                                <form action="process.php" method="post">
                                        <input type="hidden" name="return" value="<?php fileclass();?>">
                                        <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                                        <input type="hidden" name="process" value="update">
                                        <input type="submit" value="reset password" class="btn btn-warning">
                                    </form>
                                    <form action="process.php" method="post">
                                        <input type="hidden" name="return" value="<?php fileclass();?>">
                                        <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                                        <input type="hidden" name="process" value="delete">
                                        <input type="submit" value="remove student" class="btn btn-danger">
                                    </form>

                                    
                                </td>
                            </tr>
                            <?php 
                            }

                            
                        }
                        ?>
                        <!-- end data  -->
                    </tbody>
                    </table>


                </div>
              </div>
                <!-- end list  -->
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
  <style>
    label, th {
    text-transform: capitalize;
}
  </style>
  <?php include('footer.php');?>