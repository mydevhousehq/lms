<?php include('header.php');?>

<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Lectures</h1>

      <div class="row">
        
        <?php 
        $data = custom_query('select * from tbl_lecture order by phase asc');;
        foreach ($data as $row) {
          ?>
              <div class="col-md-6 close-btn-this">
          
                <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                          <h6 class="m-0 font-weight-bold text-primary">Week: <?php echo $row['phase'];?></h6>
                          <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            
                                  <!-- DROPDOWN --> 
 <!-- <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a> -->
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item close-btn" href="#">close</a>
                      <form action="process.php" method="post">
                        <input type="hidden" name="process" value="delete">
                        <input type="hidden" name="return" value="<?php fileclass();?>">
                        <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                        <?php 
                          if($access=="admin"){
                            ?>
                              <input type="submit" class="dropdown-item" value="delete ( admin option )">
                            <?php 
                          }
                        ?>
                        
                      </form>
                                  <!-- end DP -->
                                
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">

                            <div class="row">
                            <!-- content  -->
                            <div class="col-md-6">
                            <h6>Description</h6>
                            <p><?php echo $row['content'];?></p>

                            <!-- buttons -->
                            <a class="btn btn-danger btn-pin btn-bg1" href="../api/uploads/<?php echo $row['file'];?>">Module</a>
                            <a class="btn btn-success btn-pin btn-bg2" href="../api/uploads/<?php echo $row['file2'];?>">Presentation</a>
                            
                          
                            <!-- end buttons  -->
                            </div>

                            <!-- video  -->
                            <div class="col-md-6">
                                <!-- <h5>Video</h5> -->
                                <div class='embed-container'>
                                <?php 
                                
                                $link =  $row['video'];;
                                $link =  str_replace('watch?v=','embed/',$link);
                                ?>
                                <iframe width="560" height="315" src="<?php echo $link;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            </div>
                <!-- end card body -->
                        </div>
                </div>
              
              </div>
          <?php 
        }
        ?>
        </div>
      <?php 
      if($access=="admin"){
        ?>


       


        <!-- add lecture  -->
        <div class="row">
            <div class="col-md-12">
            <h3>Add Lecture</h3>
            <hr>
            <form action="process.php" enctype="multipart/form-data"  method="post">
            <input type="hidden" name="return"  value="<?php fileclass();?>">
            <input type="hidden" name="process" value="add">
            <div class="form-group">
              <label for="">Week</label>
              <input type="number" class="form-control" name="phase" min="1" max="14" required>
            </div>

            <div class="form-group">
              <label for="">Content</label>
              <textarea name="content" class="form-control" id="" cols="30" rows="5"></textarea>
            </div>
            

            <div class="form-group">
              <label for="">Video</label>
              <input id="avatar-1" name="video" type="text" class="form-control file-loading" required>
            </div>

            <div class="form-group">
              <label for="">Module</label>
              <input id="avatar-1" name="file" type="file" class="file-loading" required>
            </div>

            <div class="form-group">
              <label for="">Presentation</label>
              <input id="avatar-1" name="file2" type="file" class="file-loading" >
            </div>

            <button type="submit" class="btn btn-primary mb-2">Add Lecture</button>
          </form>
            </div>
        </div>
        <!-- end add lecture  -->
        <?php 
      }else{}
      ?>
    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
<style>

.text-primary {
    color: #fff !important;
}
.card-header.py-3.d-flex.flex-row.align-items-center.justify-content-between {
    background: #000;
    
}
</style>
  <?php include('footer.php');?>