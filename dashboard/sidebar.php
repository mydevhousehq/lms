   
    <?php 

$data = get_where_fieldvalue('tbl_user','user',$_SESSION['user']);
foreach ($data as $row) {
    // echo $row['access']."<br />\n";
    $access  = $row['access'];
}
    ?>
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="./">
        <div class="sidebar-brand-icon rotate-n-15">
          <!-- <i class="fas fa-laugh-wink"></i> -->
        </div>
        <div class="sidebar-brand-text mx-3">NSTP COURSEWARE</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="./">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

     
      <?php 
      
      if($access == "admin"){
      ?>
      <div class="sidebar-heading">
       Teacher Management
      </div>
      <li class="nav-item">
        <a class="nav-link" href="lecture.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Lecture</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="exam.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Exam</span></a>
      </li>


      <li class="nav-item">
        <a class="nav-link" href="student_management.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Student Management</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="announcement.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Announcement</span></a>
      </li>
      

      <li class="nav-item">
        <a class="nav-link" href="grades.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Grades</span></a>
      </li>

      <?php 
      }elseif($access=="student"){
        //student
        ?>
        <div class="sidebar-heading">
        student section
        </div>

        <li class="nav-item">
        <a class="nav-link" href="lecture.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Lecture</span></a>
      </li>


      <li class="nav-item">
        <a class="nav-link" href="test.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Exam / Quiz</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="grades.php">
          <i class="fas fa-fw fa-table"></i>
          <span>My Grade</span></a>
      </li>
        <?php 
      }else{
?>
        <div class="sidebar-heading">
        Admin Teacher Manager
        </div>


        <li class="nav-item">
        <a class="nav-link" href="teacher_management.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Teacher Management</span></a>
        </li>


<?php 

      }
      
      ?>
     
     

      <!-- Nav Item - Charts -->
      <!-- <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fas fa-fw fa-table"></i>
          <span>menu</span></a>
      </li> -->


      <!-- Divider -->
      <!-- <hr class="sidebar-divider d-none d-md-block"> -->


     


      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->