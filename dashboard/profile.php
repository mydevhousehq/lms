<?php include('header.php');?>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Profile</h1>

        <div class="row">

        <div class="col-md-6">
          <!-- <?php echo $userid;?> -->


                <div class="card shadow mb-4">
                
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Profile</h6>
                  <div class="dropdown no-arrow">
                    
                  </div>
                </div>
                
                <div class="card-body">
                <!-- card body  -->
                  <form action="process.php" method="post">
                    <input type="hidden" name="process" value="changepass">
                    <input type="hidden" name="userid" value="<?php echo $userid;?>">
                    <input type="hidden" name="return" value="<?php fileclass();?>">
                    <label for="">Name: <?php echo $name;?></label>
                    <br>
                    <label for="">Type new password</label>
                    <input type="password" name="pass" class="form-control">
                    <br>
                    <input type="submit" value="submit" class="btn btn-info">
                  </form>
                <!-- end card body  -->
                </div>
                </div>


        </div>

            <!-- admin add -->
			<?php if($access=="developer"){
				?>
				<div class="col-md-6">
                 
            <div class="card shadow mb-4">
                
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Administrator</h6>
                  <div class="dropdown no-arrow">
                    
                  </div>
                </div>
                
                <div class="card-body">
                  
                <form action="process.php" method="post">
                <input type="hidden" name="process" value="add_admin">
                <input type="hidden" name="return" value="<?php fileclass();?>">
                

                <div class="form-group">
                    <label for="">username:</label>
                    <input type="text" class="form-control" name="user" required>
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" name="pass" required>
                </div>

                <div class="form-group">
                    <label for="pwd">name:</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                
                <button type="submit" class="btn btn-info">Submit</button>
                </form>
               


                </div>
              </div>
            
            </div>
				<?php 
			}?>
			<!--end admin add -->
        </div>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
  <style>
    label, th {
    text-transform: capitalize;
}
  </style>
  <?php include('footer.php');?>