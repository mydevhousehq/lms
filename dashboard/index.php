<?php include('header.php');

?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Dashboard</h1>
      <!-- <?php fileclass();?> -->
      <!-- <?php echo $access;?> -->
      <?php 
        if($access=="student"){
          ?>
            <!-- student notification   -->
            <div class="row">

            <div class="col-xl-3 col-md-4 mb-4" style="height:120px;">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">You can download the mobile lecture view here</div>
                        <a class="btn btn-info btn-lg" href="../apk/app-debug.apk" download>Download APK Here </a>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

              <div class="col-md-8">
                <!-- start  -->
                <h3>Announcement View</h3>
                <?php 
                // custom query usage
                $data = custom_query("select * from 	tbl_announcement	order by id desc limit 10");
                foreach ($data as $row) {
                    // echo $row['id']."<br />\n";
                    ?>
                        <div class="card bg-light text-black shadow" style="margin-top:20px;">
                            <div class="card-body">
                            <h4><?php echo $row['title'];?></h4>
                            <h2><?php echo $row['descript'];?></h2>
                            <h5><?php echo $row['date'];?></h5>
                            <!-- <div class="text-black-50 small">#f8f9fc</div> -->
                            </div>
                        </div>
                    <?php 
                }
                ?>
                <!-- end  -->
              </div>

            </div>
            <!-- end student notif row  -->
          <?php 
        }else{}
      ?>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

  <?php include('footer.php');?>