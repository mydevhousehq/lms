<?php include('header.php');?>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Announcement Page</h1>

        <div class="row">
                
                <!-- announcement add  -->
                <div class="col-md-4">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Add Announcement</h6>
                        </div>
                        <div class="card-body">

                            <form action="process.php" method="post">
                                <input type="hidden" name="return" value="<?php fileclass();?>">
                                <input type="hidden" value="add" name="process">
                                <label for="">Title</label>
                                <input type="text" name="title" class="form-control" required> 
                                <label for="">Announcement</label>
                                <textarea name="descript" class="form-control" id="" cols="30" rows="10" required></textarea>
                                <label for="">Date</label>
                                <input type="date" name="date" class="form-control" min="<?php echo date('Y-m-d');?>" required>
                                <br>
                                <input type="submit" value="submit">
                            </form>

                        </div>
                    </div>
                </div>

                <!-- announcement view  -->
                <div class="col-md-8">
                <h3>Announcement View</h3>
                <br>
                <?php 
                if(isset($_GET['id'])){
                    if(delete($_GET['id'],'tbl_announcement')){
                    	?>
                        <div class="card bg-light text-black shadow" style="margin-top:20px;">
                            <div class="card-body">
                                <h3>announcement deleted</h3>
                            </div>
                            </div>
                        <?php 
                    }else{
                    	echo 'echo on query';
                    }
                }
                ?>

                <?php 
                // custom query usage
                $data = custom_query("select * from 	tbl_announcement	order by id desc limit 10");
                foreach ($data as $row) {
                    // echo $row['id']."<br />\n";
                    ?>
                        <div class="card bg-light text-black shadow" style="margin-top:20px;">
                            <div class="card-body">
                            <h4><?php echo $row['title'];?></h4>
                            <h2><?php echo $row['descript'];?></h2>
                            <h5><?php echo $row['date'];?></h5>
                            <!-- <div class="text-black-50 small">#f8f9fc</div> -->
                            <a href="?id=<?php echo $row['id'];?>" class="btn btn-danger">delete</a>
                            </div>
                        </div>
                    <?php 
                }
                ?>
                </div>
        <!-- end row  -->
        </div>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

  <?php include('footer.php');?>