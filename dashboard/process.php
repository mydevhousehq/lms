<?php include('header.php');?>



<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

   <?php include('topbar.php');?>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
    
      <h1 class="h3 mb-4 text-gray-800">Process</h1>

      <?php 
      if($_SERVER['REQUEST_METHOD']=="POST"){

        if(isset($_SESSION['user'])){
    //LECTURE
      if($_POST['return']=="lecture"){
          //start add
        if($_POST['process']=="add"){
              //Lecture Add
          $str= "select * from tbl_lecture where phase=:p";
          $cm=$conn->prepare($str);
          $cm->bindParam(':p', $_POST['phase']);
        
          $cm->execute();
          $count = $cm->rowcount();
          
          if ($count == 0) {
            $fileName = $_FILES['file']['name'];
            $tmpName = $_FILES['file']['tmp_name'];
            $fileSize = $_FILES['file']['size'];
            $fileType = $_FILES['file']['type'];
            $path1 =  $path . $fileName;
            $mv = move_uploaded_file($tmpName, $path1);

            $fileName2 = $_FILES['file2']['name'];
            $tmpName2 = $_FILES['file2']['tmp_name'];
            $fileSize2 = $_FILES['file2']['size'];
            $fileType2 = $_FILES['file2']['type'];
            $path2 =  $path . $fileName2;
            $mv2 = move_uploaded_file($tmpName2, $path2);


            if($mv){
              if($mv2){
                $array = array(
                  'phase'=>$_POST['phase'],
                  'content'=>$_POST['content'],
                  'video'=>$_POST['video'],
                  'file'=>$fileName,
                  'file2'=>$fileName2
              );
              if(insert($array,'tbl_lecture')){
                  ?>
                  <script>alert('success');
                  window.location.href = '<?php echo $_POST['return'];?>.php';
                  </script>
                  <?php 
              }else{
                  ?>
                  <script>alert('failed');
                  window.location.href = '<?php echo $_POST['return'];?>.php';
                  </script>
                  <?php 
              }
              }else{
                
              }
              
                
            }else{

            }
            
          }else{
        
            ?>
            <script>alert('Week already exist');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php
        
          }
          //end add
        }  


        if($_POST['process']=="delete"){
          // delete function usage
          if(delete($_POST['id'],'tbl_lecture')){
            ?>
            <script>alert('lecture removed');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php
          }else{
          	echo 'echo on query';
          }
        }
        
      }
    //     
      
    


//student management
      if($_POST['return']=="student_management"){
        //
        if($_POST['process']=="add"){
          $str= "select * from tbl_user where student_id=:p";
          $cm=$conn->prepare($str);
          $cm->bindParam(':p', $_POST['sid']);
          $cm->execute();
          $count = $cm->rowcount();
          if ($count == 0){
            // insert function usage
            $array = array(
              'user'=>$_POST['user'],
              'pass'=>md5($_POST['pass']),
              'student_id'=>$_POST['sid'],
              'name'=> $_POST['name'],
              'status'=>'offline',
              'access'=>'student'
            );
            if(insert($array,'tbl_user')){
              ?>
            <script>alert('student enrolled');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php 
            }else{
            	echo 'error on insert';
            }
            //end
          }else{
            ?>
            <script>alert('student id already exist');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php 
          }
        }
        //
        if($_POST['process']=="delete"){
          echo $_POST['id'];
          if(delete($_POST['id'],'tbl_user')){
          	?>
            <script>alert('student deleted');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php 
          }else{
          	echo 'echo on query';
          }
        }
        //
        $md5123=md5('123');
        if($_POST['process']=="update"){
          $array = array(
          	'pass'=> $md5123
          );
          if(update($array,$_POST['id'],'tbl_user')){
          	?>
            <script>alert('password reset');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php 
          }else{
          	echo 'error on update';
          }
        }
        //
      }

//end student management


//announcement
if($_POST['return']=="announcement"){

    if($_POST['process']=="add"){
        // insert function usage
        $array = array(
          'title'=>$_POST['title'],
          'descript'=>$_POST['descript'],
          'date'=>$_POST['date']
        );
        if(insert($array,'tbl_announcement')){
        	  ?>
            <script>alert('Announced');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php 
        }else{
        	echo 'error on insert';
        }
    }

    if($_POST['process']=="delete"){}

}
//end announcement
//exam
if($_POST['return']=="exam"){
  //
  if($_POST['process']=="add_exam"){
    $conn=getconnection();
    $str= "select * from tbl_exam where type=:u";
    $cm=$conn->prepare($str);
    $cm->bindParam(':u', $_POST['type']);
    $cm->execute();
    $count = $cm->rowcount();
    if ($count == 0) {
      $array = array(
        'title'=>$_POST['title'],
        'type'=>$_POST['type']
      );
      if(insert($array,'tbl_exam')){
      	?>
        <script>alert('Test Added');
        window.location.href = '<?php echo $_POST['return'];?>.php';
        </script>
        <?php
      }else{
      	echo 'error on insert';
      }
    }else{
      ?>
      <script>alert('Already Exist');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php
    }
    //
  }
  if($_POST['process']=="add_item"){
      $array = array(
        'qid'=>$_POST['id'],
        'question'=>$_POST['question'],
        'correct'=>$_POST['correct'],
        'a'=>$_POST['a'],
        'b'=>$_POST['b'],
        'c'=>$_POST['c'],
        'd'=>$_POST['d']
      );
      if(insert($array,'tbl_exam_item')){
      	?>
        <script>alert('Item Added');
        window.location.href = '<?php echo $_POST['return'];?>.php?id=<?php echo $_POST['id'];?>&type=Q1&view=itemview&title=<?php echo $_POST['title'];?>';
        </script>
        <?php
      }else{
      	echo 'error on insert';
      }
  }
  //
}



//
if($_POST['return']=="test"){
  if($_POST['process']=="exam"){
    //
    ?>
    <h2>student: <?php echo $student_id;?></h2>
    <?php 
    foreach($_POST['id'] as $keyPost => $valuePost){ 
      // echo $_POST['answer'][$keyPost]."correct= ".$_POST['correct'][$keyPost]."<br>";
      // echo $_POST['id'][$keyPost]."<br>";
      // insert function usage
      $array = array(
        'qid'=>$_POST['qid'],
        'item_id'=>$_POST['id'][$keyPost],
        'student_answer'=>$_POST['answer'][$keyPost],
        'correct_answer'=>$_POST['correct'][$keyPost],
        'student_id'=>$_POST['student_id']
      );
      if(insert($array,'tbl_student_answer')){
      	// echo 'answered question ID   '.$_POST['id'][$keyPost]."<br>";
      }else{
      	echo 'error on insert';
      }
     }
    //
    ?>
    <script>alert('Exam done');</script>
    <a href="test.php" class="btn btn-success"> back to test page </a>
    <?php 
  }
}
//


//
if($_POST['return']=="profile"){
  if($_POST['process']=="changepass"){
    // update function usage
    $array = array(
    	'pass'=> md5($_POST['pass'])
    );
    if(update($array,$_POST['userid'],'tbl_user')){
      ?>
      <script>alert('Password Updated');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php
    }else{
    	echo 'error on update';
    }
  }
  if($_POST['process']=="add_admin"){


    $str= "select * from tbl_user where user=:u";
    $cm=$conn->prepare($str);
    $cm->bindParam(':u', $_POST['user']);
    $cm->execute();
    $user = $cm->rowcount();
    
    if ($user == 0) {
    // insert function usage
    $array = array(
      'user'=>$_POST['user'],
      'pass'=>md5($_POST['pass']),
      'name'=>$_POST['name'],
      'access'=>'admin'
    );
    if(insert($array,'tbl_user')){
      ?>
      <script>alert('Admin account created');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php
    }else{
    	echo 'error on insert';
    }
    }else{
      ?>
      <script>alert('Admin username already exist');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php
    }

    // insert function usage
    // $array = array(
    //   'user'=>$_POST['user'],
    //   'pass'=>$_POST['pass'],
    //   'name'=>$_POST['name']
    // );
    // if(insert($array,'tbl_user')){
    // 	echo 'inserted';
    // }else{
    // 	echo 'error on insert';
    // }
  }
}
//



if($_POST['return']=="teacher_management"){
  //
  if($_POST['process']=="add"){
    $str= "select * from tbl_user where user=:p";
    $cm=$conn->prepare($str);
    $cm->bindParam(':p', $_POST['user']);
    $cm->execute();
    $count = $cm->rowcount();
    if ($count == 0){
      // insert function usage
      $array = array(
        'user'=>$_POST['user'],
        'pass'=>md5($_POST['pass']),
        'student_id'=>$_POST['sid'],
        'name'=> $_POST['name'],
        'status'=>'offline',
        'access'=>'admin'
      );
      if(insert($array,'tbl_user')){
        ?>
      <script>alert('Successfully registered teacher');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php 
      }else{
        echo 'error on insert';
      }
      //end
    }else{
      ?>
      <script>alert('username already exist');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php 
    }
  }
  //
  if($_POST['process']=="delete"){
    echo $_POST['id'];
    if(delete($_POST['id'],'tbl_user')){
      ?>
      <script>alert('Teacher removed');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php 
    }else{
      echo 'echo on query';
    }
  }
  //
  $md5123=md5('123');
  if($_POST['process']=="update"){
    $array = array(
      'pass'=> $md5123
    );
    if(update($array,$_POST['id'],'tbl_user')){
      ?>
      <script>alert('password reset');
      window.location.href = '<?php echo $_POST['return'];?>.php';
      </script>
      <?php 
    }else{
      echo 'error on update';
    }
  }
  //
}
}else{
  die();
}
      //post method
      }

    
      ?>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

  <?php include('footer.php');?>